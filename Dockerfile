FROM ruby:2.4.6

RUN apt-get update
RUN gem install bundler

ENV HOME=/home/matrix
RUN mkdir -p $HOME
ADD . $HOME
WORKDIR $HOME

RUN bundle install

ARG SOUL=Smith
ENV USER ${SOUL}

RUN useradd -ms /bin/bash $USER

USER $USER
