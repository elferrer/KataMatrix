require 'yaml'

class Config
  CONFIGURATION = YAML.load(File.open('config/matrix_config.yml').read)

  def self.env
    data = {
      :garage => 'machines',
      :ip_expression => '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}',
      :mac_expression => '([[:alnum:]]{1,2}.){5}[[:alnum:]]{1,2}',
      :generic_name_of_a_docker_container => CONFIGURATION['generic_name_of_a_docker_container'],
      :folderAtHost => CONFIGURATION['folderAtHost'],
      :folderAtGuest => CONFIGURATION['folderAtGuest'],
      :name_virt_network => CONFIGURATION['name_virt_network'],
      :matrix_user_one => CONFIGURATION['matrix_user_one'],
      :cluster_user => CONFIGURATION['cluster_user'],
      :machine_template => CONFIGURATION['keysring_of_keymaker']['name'],
      :template_QCowDisk => CONFIGURATION['keysring_of_keymaker']['sda'],
      :masterCluster => CONFIGURATION['masterCluster'],
      :masterCluster_disk => CONFIGURATION['neo_machine']['sda'],
      :nodeClusterOne => CONFIGURATION['neo_console_one']['name'],
      :nodeClusterOne_disk => CONFIGURATION['neo_console_one']['sda'],
      :nodeClusterTwo => CONFIGURATION['neo_console_two']['name'],
      :nodeClusterTwo_disk => CONFIGURATION['neo_console_two']['sda'],
      :nodeClusterThree => CONFIGURATION['neo_console_three']['name'],
      :nodeClusterThree_disk => CONFIGURATION['neo_console_three']['sda'],
      :liveCD => CONFIGURATION['liveCD']
    }
    return data
  end
end
