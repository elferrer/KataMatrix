class SpecConfig
  def self.env
    data = {
      :safe_box => ".ssh",

      :user_of_masterCluster_key => "neo",
      :masterCluster_key => "neo_machine_key",

      :permissions_dir_700 => "40700",
      :permissions_file_600 => "100600",
      :permissions_file_644 => "100644",
      :key_is_Ok => "SSH_OK",
      :key_is_NOT_Ok => "SSH_NOK"
    }
    return data
  end
end
