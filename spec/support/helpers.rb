module Helpers

  def load_docker_working_directory
    `docker inspect #{Config.env[:generic_name_of_a_docker_container]} | grep -m1 "WorkingDir" | cut -d '"' -f 4`
  end

  def build_a_docker_image
    puts "    ·\033[33m(building docker image... it will take a while...)"
    system("docker build -t #{Config.env[:generic_name_of_a_docker_container]} . > /dev/null")
  end

  def list_of_vm
    `virsh list --all`
  end

  def list_of_shutoff_vm
    `virsh list --state-shutoff`
  end

  def list_of_running_vm
    `virsh list --state-running`
  end

  def reboot_virtual_machine(the_machine)
    `virsh reboot #{the_machine}`
  end

  def get_the_ip_from_hosts(the_machine)
    ip_and_host = get_entry_of_host the_machine
    ip = ip_and_host[/(#{Config.env[:ip_expression]}) (.*)/,1]
    return ip
  end

  def get_entry_of_host(the_machine)
    `getent hosts #{the_machine}`
  end

  def reboot_vm_and_return_a_message(message,the_machine,virtual_network)
    reboot_virtual_machine the_machine
    puts "\033[33m      Rebooting the '#{the_machine}' "
    puts "\033[33m      Cause '#{message}' "
    print "\033[33m      Rebooting "
    waiting_read_ip_of_vm(the_machine,virtual_network)
    print "\033[33m ok! \n"
  end

  def reconnect_to_vm_and_return_a_message(message,the_machine,virtual_network,key,remote_user)
    puts "\033[33m      Reconnecting to '#{the_machine}' "
    puts "\033[33m      Cause '#{message}' "
    print "\033[33m      Trying to connect "
    waiting_for_ssh_connection(the_machine,virtual_network,key,remote_user)
    print "\033[33m ok! \n"
  end

  def waiting_read_ip_of_vm(the_machine,virtual_network)
    ip_address = get_ip_from(the_machine,virtual_network)
    count_and_sleep = reset_counter
    while ip_address == :failure_in_name_resolution
      count_and_sleep += sleeping seconds_to_await
      ip_address = get_ip_from(the_machine,virtual_network)
      break if count_and_sleep >= limit_time_to_retry_connection
    end
  end

  def waiting_for_ssh_connection(the_machine,virtual_network,key,remote_user)
    the_mac_address = get_ip_from(the_machine,virtual_network)
    trying_open_door = test_ssh_connection_to_vm(the_mac_address,key,remote_user)
    door_is_open = false unless trying_open_door.include? SpecConfig.env[:key_is_Ok]
    count_and_sleep = reset_counter
    while !door_is_open
      count_and_sleep += sleeping seconds_to_await
      trying_open_door = ""
      trying_open_door = test_ssh_connection_to_vm(the_mac_address,key,remote_user)
      door_is_open = ""
      door_is_open = false unless trying_open_door.include? SpecConfig.env[:key_is_Ok]
      break if count_and_sleep >= limit_time_to_retry_connection
    end
  end

  def launch_shellsentence_to_ssh_connection(key,remote_user,ip_address,shell_sentence)
    `ssh -i #{key} -q -o 'BatchMode=yes' #{remote_user}@#{ip_address} #{shell_sentence}`
  end

  def ask_permissions_of_ssh_connection(ip_address,remote_user)
    shell_sentence = "echo 2>&1"
    `ssh -o 'BatchMode=yes' -o 'ConnectTimeout=5' #{remote_user}@#{ip_address} #{shell_sentence}`
  end

  def test_ssh_connection_to_vm(the_mac_address,key,remote_user)
    the_mac_address = the_mac_address.to_s
    shell_sentence = "'echo 2>&1' && echo #{SpecConfig.env[:key_is_Ok]} || echo #{SpecConfig.env[:key_is_NOT_Ok]}"
    connection_availability = launch_shellsentence_to_ssh_connection(key,remote_user,the_mac_address,shell_sentence)
    return connection_availability
  end

  def copy_go_through_a_list(objects_list,new_origin_object,destination_object)
    objects_list.each do |object|
      origin_object = [new_origin_object,object].join('/')
      new_destination = [destination_object,object].join('/')
      (copy_file(origin_object,new_destination) ; next) if File.file?(object)
      current_directory = Dir.pwd
      copy_directory(origin_object,new_destination) if File.directory?(object)
      Dir.chdir(current_directory)
    end
  end

  def remove_directory(directory)
    if File.directory?(directory)
      Dir.foreach(directory) do |object|
        remove_directory("#{directory}/#{object}") if ((object.to_s != ".") and (object.to_s != ".."))
      end
      Dir.delete(directory)
    else
      File.delete(directory)
    end
  end

  def get_ip_from(the_machine,virtual_network)
    the_ip_address = get_ip_address(the_machine,virtual_network)
    return :failure_in_name_resolution if the_ip_address.nil?
    the_ip_address
  end

  private

  def get_ip_address(the_machine,virtual_network)
    ip_address_list = get_vm_ip_list(virtual_network)
    a_mac_address = get_mac_address_list the_machine
    get_the_ip_from_a_list ip_address_list,a_mac_address
  end

  def get_the_ip_from_a_list(ip_address_list,a_mac_address)
    their_ip_and_mac = (ip_address_list.match /#{a_mac_address}\s*ipv4\s*(#{Config.env[:ip_expression]})/).to_s
    the_ip = their_ip_and_mac[/(.*) (#{Config.env[:ip_expression]})/,2]
    the_ip
  end

  def get_vm_ip_list(virtual_network)
    `virsh net-dhcp-leases "#{virtual_network}"`
  end

  def get_mac_address_list(the_machine)
    `virsh domiflist --domain #{the_machine} | egrep -o '#{Config.env[:mac_expression]}' | tr -d '\n'`
  end

  def reset_counter
    OutsideVirtualizationConfig.env[:reset]
  end

  def seconds_to_await
    OutsideVirtualizationConfig.env[:seconds_to_await]
  end

  def limit_time_to_retry_connection
    OutsideVirtualizationConfig.env[:limit_time_to_retry_connection]
  end

  def sleeping(seconds)
    sleep seconds
    print "\033[33m."
    return seconds
  end

  def copy_file(origin_object,destination_object)
    File.write(destination_object, File.read(origin_object))
  end

  def copy_directory(origin_object,destination_object)
    create_new_directory(destination_object)
    Dir.chdir(origin_object)
    new_origin_object = Dir.pwd
    objects_list = Dir.glob('*')
    copy_go_through_a_list(objects_list,new_origin_object,destination_object)
  end

  def create_new_directory(destination_object)
    Dir.mkdir(destination_object) unless Dir.exist?(destination_object)
  end

end
