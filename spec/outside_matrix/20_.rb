describe "20. The virtual machine '#{Config.env[:masterCluster]}'"  do
  virtual_network = Config.env[:name_virt_network]
  the_machine = Config.env[:masterCluster]
  key = SpecConfig.env[:user_of_masterCluster_key]
  before(:all) do
    @softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
  end

  case
  when list_of_shutoff_vm.include?(the_machine)
    it "·\033[33m(is shut off)\033[0m" do

      expect(list_of_shutoff_vm).to include OutsideVirtualizationConfig.env[:VM_is_off]
    end

  when list_of_running_vm.include?(the_machine)
    it "is running" do
      ip_address = get_ip_from(the_machine,virtual_network)

      expect(list_of_running_vm).to include OutsideVirtualizationConfig.env[:VM_is_running]
    end

    context "exist" do
      it "and only reboot if necessary" do
        ip_address = get_ip_from(the_machine,virtual_network)

        reboot_vm_and_return_a_message(:failure_in_name_resolution,the_machine,virtual_network) if ip_address == :failure_in_name_resolution
      end

      it "and only trying to reconnect if necessary" do
        remote_user = Config.env[:cluster_user]
        ip_of_the_machine = get_ip_from(the_machine,virtual_network)

        trying_open_door = test_ssh_connection_to_vm(ip_of_the_machine,@softkey,remote_user)

        reconnect_to_vm_and_return_a_message("I have lost SSH connection",the_machine,virtual_network,@softkey,remote_user) unless trying_open_door.include?(SpecConfig.env[:key_is_Ok])
      end

      it "and their connection is alive" do
        ip_address = get_ip_from(the_machine,virtual_network)

        expect(ip_address).to_not equal :failure_in_name_resolution
      end
    end

    context "have installed" do
      it "Ruby" do
        ip_address = get_ip_from(the_machine,virtual_network)
        remote_user = Config.env[:cluster_user]

        shell_sentence = "ruby -v"
        ask_version = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

        expect(ask_version).to include "ruby"
      end

      it "RSpec" do
        ip_address = get_ip_from(the_machine,virtual_network)
        remote_user = Config.env[:cluster_user]

        shell_sentence = "rspec -v"
        ask_version = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

        expect(ask_version).to include "RSpec"
      end

      it "Bundler" do
        ip_address = get_ip_from(the_machine,virtual_network)
        remote_user = Config.env[:cluster_user]

        shell_sentence = "bundle -v"
        ask_version = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

        expect(ask_version).to include "Bundler"
      end
    end
  end
end
