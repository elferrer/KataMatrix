describe "10. The machine '#{Config.env[:machine_template]}' "  do
  virtual_network = Config.env[:name_virt_network]
  the_machine = Config.env[:machine_template]
  key = OutsideVirtualizationConfig.env[:machine_template_key]
  before(:all) do
    @softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
  end

  case
  when list_of_shutoff_vm.include?(the_machine)
    it "·\033[33m(is shut off)\033[0m" do

      expect(list_of_shutoff_vm).to include OutsideVirtualizationConfig.env[:VM_is_off]
    end

  when list_of_running_vm.include?(the_machine)
    it "is running" do
      ip_address = get_ip_from(the_machine,virtual_network)

      expect(list_of_running_vm).to include OutsideVirtualizationConfig.env[:VM_is_running]
    end

    context "exist" do
      it "and only reboot if necessary" do
        ip_address = get_ip_from(the_machine,virtual_network)

        reboot_vm_and_return_a_message(:failure_in_name_resolution,the_machine,virtual_network) if ip_address == :failure_in_name_resolution
      end

      it "and only trying to reconnect if necessary" do
        remote_user = Config.env[:cluster_user]
        ip_of_the_machine = get_ip_from(the_machine,virtual_network)

        trying_open_door = test_ssh_connection_to_vm(ip_of_the_machine,@softkey,remote_user)

        reconnect_to_vm_and_return_a_message("I have lost SSH connection",the_machine,virtual_network,@softkey,remote_user) unless trying_open_door.include?(SpecConfig.env[:key_is_Ok])
      end

      it "and their connection is alive" do
        ip_address = get_ip_from(the_machine,virtual_network)

        expect(ip_address).to_not equal :failure_in_name_resolution
      end
    end

    context "and it have" do
      it "a dns nameservers" do
        ip_address = get_ip_from(the_machine,virtual_network)
        expected_nameservers = "- 8.8.8.8\n- 8.8.4.4\n\n"

        remote_user = Config.env[:cluster_user]
        shell_sentence = "'sudo ros config get rancher.network.dns.nameservers'"
        ask_nameservers = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

        expect(ask_nameservers).to eq expected_nameservers
      end

      it "a valid gateway" do
        ip_address = get_ip_from(the_machine,virtual_network)
        remote_user = Config.env[:cluster_user]

        shell_sentence = "\"/sbin/ip route | awk '/#{Config.env[:name_virt_network]}/ { print $3 }'\""
        ask_gateway = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)
        expected_gateway = ask_gateway.match(/#{Config.env[:ip_expression]}/).to_s
        expected_gateway = "Machine off" if expected_gateway == ""
        shell_sentence = "'sudo ros config get rancher.network.interfaces.eth0.gateway'"
        configured_gateway = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

        expect(configured_gateway).to include expected_gateway
      end

      it "a valid address ip" do
        ip_address = get_ip_from(the_machine,virtual_network)
        ip_range = "#{ip_address}/24\n"

        remote_user = Config.env[:cluster_user]
        shell_sentence = "'sudo ros config get rancher.network.interfaces.eth0.address'"
        configured_ip = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

        expect(configured_ip).to eq ip_range
      end

      it "a correct docker engine" do
        ip_address = get_ip_from(the_machine,virtual_network)
        docker_engine = "current  docker-18.06.3-ce\n"

        remote_user = Config.env[:cluster_user]
        shell_sentence = "\"sudo ros engine list | grep 'current'\""
        configured_engine = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

        expect(configured_engine).to eq docker_engine
      end
    end
  end
end
