describe "7. The VM machine '#{Config.env[:machine_template]}'" do
  it "has a original hard disk ('#{Config.env[:template_QCowDisk]}')" do
    hardkey = Config.env[:template_QCowDisk]
    Dir.chdir(Config.env[:garage])

    content = Dir.glob(hardkey)
    Dir.chdir('..')

    expect(content[0]).to eq hardkey
  end

  it "is created" do
    the_machine = Config.env[:machine_template]

    expect(list_of_vm).to include the_machine
  end

  it "has a raw copy of '#{OutsideVirtualizationConfig.env[:template_RawDisk]}' " do
    hardkey = OutsideVirtualizationConfig.env[:template_RawDisk]
    Dir.chdir(Config.env[:garage])

    content = Dir.glob(hardkey)
    Dir.chdir('..')

    expect(content[0]).to eq hardkey
  end
end
