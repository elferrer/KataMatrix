describe "2. The docker world" do
  context "has installed some programs in a machine:" do
    installed_programs = ['docker', 'docker-compose']

    installed_programs.each do |program|
      it "the #{program} is installed" do

        found_program = system("which #{program} > /dev/null")

        expect(found_program).to be true
      end
    end
  end

  context "exist and" do
    it "connection is possible" do
      connection = exist_docker_connection?

      expect(connection).to be true
    end

    it "has /home/matrix as workdir" , :docker => true do
      build_a_docker_image

      working_connection = load_docker_working_directory

      expect(working_connection).to eq "/home/matrix\n"
    end

    it "is protected by .dockerignore" do
      is_protected = ".dockerignore"

      content = Dir.glob(is_protected)

      expect(content[0]).to eq is_protected
    end
  end
end
