describe "4. I have installed some programs in a machine:" do
  installed_programs = ['ansible',
    'imvirt', 'libvirtd', 'qemu-x86_64', 'virt-manager', 'virsh', 'ssh']

  installed_programs.each do |program|
    it "the #{program} is installed" do

      found_program = system("which #{program} > /dev/null")

      expect(found_program).to be true
    end
  end
end
