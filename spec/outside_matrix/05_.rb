describe "5. An ISO (machine constructor)" do
  it "is downloaded" do
    Dir.chdir(Config.env[:garage])

    content = Dir.glob(Config.env[:liveCD])
    Dir.chdir('..')

    expect(content[0]).to eq Config.env[:liveCD]
  end

  it "have got a valid checksum" do
    Dir.chdir(Config.env[:garage])

    checksum = `grep 'md5:' #{OutsideVirtualizationConfig.env[:liveCD_checksum]}`
    machine_check = "md5: " + `md5sum #{Config.env[:liveCD]}`
    Dir.chdir('..')

    expect(machine_check).to eq checksum
  end
end
