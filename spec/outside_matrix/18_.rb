describe "18. Host system" do
  it "have 'sshfs' connector" do
    program = 'sshfs'

    found_program = system("which #{program} > /dev/null")

    expect(found_program).to be true
  end

  it "it's configured to allow 'sshfs' connections" do
    user_allow_other = "user_allow_other\n"

    search_uncommented_text = `cat /etc/fuse.conf | grep "^user_allow_other"`

    expect(search_uncommented_text).to eq user_allow_other
  end
end
