describe "15. '#{Config.env[:matrix_user_one]}' has" do

  make = [Config.env[:nodeClusterOne_disk], Config.env[:nodeClusterTwo_disk], Config.env[:nodeClusterThree_disk]]
  make.each do |creation|
    it "created the '#{creation}' harddisk" do
      hardkey = "#{creation}"
      Dir.chdir(Config.env[:garage])

      content = Dir.glob(hardkey)
      Dir.chdir('..')

      expect(content[0]).to eq hardkey
    end
  end

  make = [Config.env[:nodeClusterOne], Config.env[:nodeClusterTwo], Config.env[:nodeClusterThree]]
  make.each do |creation|
    it "created the '#{creation}' machine" do
      neo_console = "#{creation}"

      expect(list_of_vm).to include neo_console
    end
  end
end
