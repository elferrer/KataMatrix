describe "11. The ssh key of user '#{Config.env[:matrix_user_one]}'" do
  key = SpecConfig.env[:user_of_masterCluster_key]
  before(:all) do
    @softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
  end

  make = ['', '.pub']
  make.each do |creation|
    it "has a file #{creation} key" do
      Dir.chdir(SpecConfig.env[:safe_box])

      content = Dir.glob("*")
      Dir.chdir('..')

      expect(content).to include key+creation
    end
  end

  it "it's in a protected directory" do
    private_key = File.new(SpecConfig.env[:safe_box])

    key_properties = private_key.stat
    file_properties = "%o" % key_properties.mode

    expect(file_properties).to eq SpecConfig.env[:permissions_dir_700]
  end

  it "has protected their ssh #{@softkey} private key"do
    private_key = File.new(@softkey)

    key_properties = private_key.stat
    file_properties = "%o" % key_properties.mode

    expect(file_properties).to eq SpecConfig.env[:permissions_file_600]
  end

  it "has protected their ssh #{@softkey} public key" do
    private_key = File.new("#{@softkey}.pub")

    key_properties = private_key.stat
    file_properties = "%o" % key_properties.mode

    expect(file_properties).to eq SpecConfig.env[:permissions_file_644]
  end
end
