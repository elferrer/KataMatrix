describe "6. The 'keymaker' user" do
  key = OutsideVirtualizationConfig.env[:machine_template_key]
  before(:all) do
    @softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
  end

  make = ['', '.pub']
  make.each do |creation|
    it "has a #{creation} key" do
      Dir.chdir(SpecConfig.env[:safe_box])

      content = Dir.glob("*")
      Dir.chdir('..')
      
      expect(content).to include key+creation
    end
  end

  it "has protected their directory" do
    private_dir = File.new(SpecConfig.env[:safe_box])

    private_properties = private_dir.stat
    dir_properties = "%o" % private_properties.mode

    expect(dir_properties).to eq SpecConfig.env[:permissions_dir_700]
  end

  it "has protected their private key '#{OutsideVirtualizationConfig.env[:machine_template_key]}' "do
    private_key = File.new(@softkey)

    key_properties = private_key.stat
    file_properties = "%o" % key_properties.mode

    expect(file_properties).to eq SpecConfig.env[:permissions_file_600]
  end

  it "has protected their the public key '#{OutsideVirtualizationConfig.env[:machine_template_key]}.pub' " do
    private_key = File.new("#{@softkey}.pub")

    key_properties = private_key.stat
    file_properties = "%o" % key_properties.mode

    expect(file_properties).to eq SpecConfig.env[:permissions_file_644]
  end
end
