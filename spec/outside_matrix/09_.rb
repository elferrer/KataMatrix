describe "9. The name of machine '#{Config.env[:machine_template]}' " do
  virtual_network = Config.env[:name_virt_network]
  the_machine = Config.env[:machine_template]
  key = OutsideVirtualizationConfig.env[:machine_template_key]
  before(:all) do
    @softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
  end

  case
  when list_of_shutoff_vm.include?(the_machine)
    it "·\033[33m(is shut off)\033[0m" do

      expect(list_of_shutoff_vm).to include OutsideVirtualizationConfig.env[:VM_is_off]
    end

  when list_of_running_vm.include?(the_machine)
    it "is running" do
      ip_address = get_ip_from(the_machine,virtual_network)

      expect(list_of_running_vm).to include OutsideVirtualizationConfig.env[:VM_is_running]
    end

    context "exist" do
      it "and their name is at the DNS of system" do
        the_hosts = "/etc/hosts"
        ip_address = get_ip_from(the_machine,virtual_network)

        to_search = `cat #{the_hosts} | grep '#{the_machine}'`

        expect(to_search).to include ip_address
        expect(to_search).to include the_machine
      end
    end
  end

  case
  when list_of_shutoff_vm.include?(the_machine)
    it "·\033[33m(is shut off)\033[0m" do

      expect(list_of_shutoff_vm).to include OutsideVirtualizationConfig.env[:VM_is_off]
    end

  when list_of_running_vm.include?(the_machine)
    it "is running" do
      ip_address = get_ip_from(the_machine,virtual_network)

      expect(list_of_running_vm).to include OutsideVirtualizationConfig.env[:VM_is_running]
    end

    context "exist" do
      it "and only reboot if necessary" do
        ip_address = get_ip_from(the_machine,virtual_network)

        reboot_vm_and_return_a_message(:failure_in_name_resolution,the_machine,virtual_network) if ip_address == :failure_in_name_resolution
      end

      it "and only trying to reconnect if necessary" do
        remote_user = Config.env[:cluster_user]
        ip_of_the_machine = get_ip_from(the_machine,virtual_network)

        trying_open_door = test_ssh_connection_to_vm(ip_of_the_machine,@softkey,remote_user)

        reconnect_to_vm_and_return_a_message("I have lost SSH connection",the_machine,virtual_network,@softkey,remote_user) unless trying_open_door.include?(SpecConfig.env[:key_is_Ok])
      end

      it "and their connection is alive" do
        ip_address = get_ip_from(the_machine,virtual_network)

        expect(ip_address).to_not equal :failure_in_name_resolution
      end
    end

    context "and the '#{Config.env[:machine_template]}' machine" do
      it "have their name in their DNS" do
        the_hosts = "/etc/hosts"

        ip_address = get_ip_from(the_machine,virtual_network)
        remote_user = Config.env[:cluster_user]
        shell_sentence = "'cat #{the_hosts}'"
        to_search = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

        expect(to_search).to include the_machine
      end
    end
  end
end
