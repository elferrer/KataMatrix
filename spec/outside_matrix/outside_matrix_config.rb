class OutsideVirtualizationConfig
  def self.env
    data = {
      :template_RawDisk => "hardkey.raw",
      :machine_template_key => "softkey",

      :liveCD_checksum => "iso-checksums.txt",

      :VM_is_running => "running",
      :VM_is_off => "shut off",

      :reset => 0,
      :seconds_to_await => 3,
      :limit_time_to_awaiting_reboot => 120,
      :limit_time_to_retry_connection => 120
    }
    return data
  end
end
