describe "19. Connection to virtual machine '#{Config.env[:masterCluster]}'" do
  virtual_network = Config.env[:name_virt_network]
  the_machine = Config.env[:masterCluster]
  key = SpecConfig.env[:user_of_masterCluster_key]
  before(:all) do
    @softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
  end

  case
  when list_of_shutoff_vm.include?(the_machine)
    it "·\033[33m(is shut off)\033[0m" do

      expect(list_of_shutoff_vm).to include OutsideVirtualizationConfig.env[:VM_is_off]
    end

  when list_of_running_vm.include?(the_machine)
    it "is running" do
      ip_address = get_ip_from(the_machine,virtual_network)

      expect(list_of_running_vm).to include OutsideVirtualizationConfig.env[:VM_is_running]
    end

    context "exist" do
      it "and only reboot if necessary" do
        ip_address = get_ip_from(the_machine,virtual_network)

        reboot_vm_and_return_a_message(:failure_in_name_resolution,the_machine,virtual_network) if ip_address == :failure_in_name_resolution
      end

      it "and only trying to reconnect if necessary" do
        remote_user = Config.env[:cluster_user]
        ip_of_the_machine = get_ip_from(the_machine,virtual_network)

        trying_open_door = test_ssh_connection_to_vm(ip_of_the_machine,@softkey,remote_user)

        reconnect_to_vm_and_return_a_message("I have lost SSH connection",the_machine,virtual_network,@softkey,remote_user) unless trying_open_door.include?(SpecConfig.env[:key_is_Ok])
      end

      it "and their connection is alive" do
        ip_address = get_ip_from(the_machine,virtual_network)

        expect(ip_address).to_not equal :failure_in_name_resolution
      end
    end

    context "from side of host" do
      it "it's possible by creating a new folder" , :upload_test_to_machine => true do
        initial_location = Dir.pwd

        three = Config.env[:folderAtHost].split("/")
        three.each do |directory|
          Dir.chdir("/") if directory == ""
          next if directory == ""
          Dir.mkdir(directory) unless Dir.exist?(directory)
          Dir.chdir(directory)
        end
        Dir.chdir(initial_location)
        check_directory = File.directory?(Config.env[:folderAtHost])

        expect(check_directory).to be true
      end
    end

    context "in side of guest" do
      case
      when list_of_shutoff_vm.include?(the_machine)
        it "·\033[33m(is shut off)\033[0m" do

          expect(list_of_shutoff_vm).to include OutsideVirtualizationConfig.env[:VM_is_off]
        end

      when list_of_running_vm.include?(the_machine)
        it "is running" do
          ip_address = get_ip_from(the_machine,virtual_network)

          expect(list_of_running_vm).to include OutsideVirtualizationConfig.env[:VM_is_running]
        end

        it "it's possible by creating a new folder" , :upload_test_to_machine => true  do
          the_machine = Config.env[:masterCluster]
          key = SpecConfig.env[:user_of_masterCluster_key]
          softkey = "#{SpecConfig.env[:safe_box]}/#{key}"

          ip_address = get_ip_from(the_machine,virtual_network)
          remote_user = Config.env[:cluster_user]
          shell_sentence = "mkdir -p #{Config.env[:folderAtGuest]}"
          trying_open_door = launch_shellsentence_to_ssh_connection(softkey,remote_user,ip_address,shell_sentence)
          shell_sentence = "ls"
          content_directory = launch_shellsentence_to_ssh_connection(softkey,remote_user,ip_address,shell_sentence)

          expect(content_directory).to include Config.env[:folderAtGuest]
        end

        it "and connection is possible" , :upload_test_to_machine => true do
          groupOwnedBeforeLink_currentDir_fromHostFolder = File.grpowned?(Config.env[:folderAtHost])
          groupOwnedBeforeLink_parentDir_fromHostFolder = File.grpowned?(Config.env[:folderAtHost])
          remote_user = Config.env[:cluster_user]
          the_machine = Config.env[:masterCluster]
          key = SpecConfig.env[:user_of_masterCluster_key]
          softkey = "#{SpecConfig.env[:safe_box]}/#{key}"

          mount_folder_with_sshfs = `sshfs -o allow_other,IdentityFile=$PWD/#{softkey} #{remote_user}@#{the_machine}:#{Config.env[:folderAtGuest]} #{Config.env[:folderAtHost]}`
          groupOwnedAfterLink_currentDir_fromHostFolder = File.grpowned?(Config.env[:folderAtHost])
          groupOwnedAfterLink_parentDir_fromHostFolder = File.grpowned?(Config.env[:folderAtHost])

          expect(groupOwnedBeforeLink_currentDir_fromHostFolder).not_to eq groupOwnedAfterLink_currentDir_fromHostFolder
          expect(groupOwnedBeforeLink_parentDir_fromHostFolder).not_to eq groupOwnedAfterLink_parentDir_fromHostFolder
        end

        it "and copy tests to VM is possible" , :upload_test_to_machine => true do
          initial_location = Dir.pwd
          copy_list = [ 'Gemfile', 'matrix.rb', '.rspec', 'matrix_config.rb', 'config', 'spec', 'config_files']
          origin_object = initial_location
          destination_object = Config.env[:folderAtHost]

          copy_go_through_a_list(copy_list,origin_object,destination_object)
          Dir.chdir(initial_location)
          remote_user = Config.env[:cluster_user]
          the_machine = Config.env[:masterCluster]
          key = SpecConfig.env[:user_of_masterCluster_key]
          softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
          ip_address = get_ip_from(the_machine,virtual_network)
          shell_sentence = "ls #{Config.env[:folderAtGuest]} -la"
          content_directory = launch_shellsentence_to_ssh_connection(softkey,remote_user,ip_address,shell_sentence)

          copy_list.each do |object|
            expect(content_directory).to include(object)
          end
        end

        it "and remove guest folder to clean environment is possible" , :remove_tests_from_machine => true  do
          the_machine = Config.env[:masterCluster]
          key = SpecConfig.env[:user_of_masterCluster_key]
          softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
          ip_address = get_ip_from(the_machine,virtual_network)
          remote_user = Config.env[:cluster_user]
          shell_sentence = "sudo rm -r #{Config.env[:folderAtGuest]}"

          remove_folder = launch_shellsentence_to_ssh_connection(softkey,remote_user,ip_address,shell_sentence)
          shell_sentence = "ls -la"
          ls_la = launch_shellsentence_to_ssh_connection(softkey,remote_user,ip_address,shell_sentence)

          expect(remove_folder).to_not include(Config.env[:folderAtGuest])
        end

        it "and unmount connection is possible" , :remove_tests_from_machine => true do
          `fusermount -u #{Config.env[:folderAtHost]}`

          is_mounted = system("fuser -M #{Config.env[:folderAtHost]} 2>&1")

          expect(is_mounted).to be false
        end
      end
    end

    context "it's cleared at host" do
      it "by removing folder" , :remove_tests_from_machine => true do

        remove_directory(Config.env[:folderAtHost])
        error = "No such file or directory @ dir_s_chdir - #{Config.env[:folderAtHost]}"

        expect{Dir.chdir(Config.env[:folderAtHost])}.to raise_error(error)
      end
    end
  end
end
