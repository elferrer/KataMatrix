describe "16. The virtual machine"  do
  virtual_network = Config.env[:name_virt_network]

  make = [Config.env[:nodeClusterOne], Config.env[:nodeClusterTwo], Config.env[:nodeClusterThree]]
  make.each do |the_machine|
    case
    when list_of_shutoff_vm.include?(the_machine)
      it "·\033[33m(#{the_machine} is shut off)\033[0m" do

        expect(list_of_shutoff_vm).to include OutsideVirtualizationConfig.env[:VM_is_off]
      end

    when list_of_running_vm.include?(the_machine)
      key = SpecConfig.env[:user_of_masterCluster_key]
      key_maker = OutsideVirtualizationConfig.env[:machine_template_key]
      before(:all) do
        @softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
        @softkey_keymaker = "#{SpecConfig.env[:safe_box]}/#{key_maker}"
      end

      it "#{the_machine} is running" do
        ip_address = get_ip_from(the_machine,virtual_network)

        expect(list_of_running_vm).to include OutsideVirtualizationConfig.env[:VM_is_running]
      end

      context "#{the_machine} exist" do
        it "and only reboot if necessary" do
          ip_address = get_ip_from(the_machine,virtual_network)

          reboot_vm_and_return_a_message(:failure_in_name_resolution,the_machine,virtual_network) if ip_address == :failure_in_name_resolution
        end

        it "and only trying to reconnect if necessary" do
          remote_user = Config.env[:cluster_user]
          ip_of_the_machine = get_ip_from(the_machine,virtual_network)

          trying_open_door = test_ssh_connection_to_vm(ip_of_the_machine,@softkey,remote_user)

          reconnect_to_vm_and_return_a_message("I have lost SSH connection",the_machine,virtual_network,@softkey,remote_user) unless trying_open_door.include?(SpecConfig.env[:key_is_Ok])
        end

        it "and their connection is alive" do
          ip_address = get_ip_from(the_machine,virtual_network)

          expect(ip_address).to_not equal :failure_in_name_resolution
        end
      end

      context "have a ssh port in the #{the_machine}" do
        it "and it's open to #{key}" do
          ip_address = get_ip_from(the_machine,virtual_network)
          remote_user = Config.env[:cluster_user]
          shell_sentence = "'echo 2>&1' && echo #{SpecConfig.env[:key_is_Ok]} || echo #{SpecConfig.env[:key_is_NOT_Ok]}"

          trying_open_door = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

          expect(trying_open_door).to include SpecConfig.env[:key_is_Ok]
        end

        it "and it's closed to key_maker" do
          ip_address = get_ip_from(the_machine,virtual_network)
          remote_user = Config.env[:cluster_user]
          shell_sentence = "'echo 2>&1' && echo #{SpecConfig.env[:key_is_Ok]} || echo #{SpecConfig.env[:key_is_NOT_Ok]}"

          trying_open_door = launch_shellsentence_to_ssh_connection(@softkey_keymaker,remote_user,ip_address,shell_sentence)

          expect(trying_open_door).to include SpecConfig.env[:key_is_NOT_Ok]
        end

        it "secure, with only publickey (without root user login)" do
          ip_address = get_ip_from(the_machine,virtual_network)
          remote_user = Config.env[:cluster_user]
          permission_only_have_a_publickey = "Permission denied (publickey)"

          check_door = ask_permissions_of_ssh_connection(ip_address,remote_user)

          expect(check_door).to include permission_only_have_a_publickey
        end
      end
    end
  end
end
