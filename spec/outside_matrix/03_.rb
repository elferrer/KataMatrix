describe "3. The user" do
  it "is not '#{Config.env[:matrix_user_one]}' " do
    user = system_user
    
    expect(user).not_to eq Config.env[:matrix_user_one]
  end
end
