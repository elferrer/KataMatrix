require_relative '../matrix'
require './spec/matrix_spec_config'

if are_running_outside_the_matrix?
  require './spec/outside_matrix/outside_matrix_config'
  require_relative 'support/helpers'
  include Helpers
  require_relative 'outside_matrix/01_'
  require_relative 'outside_matrix/02_'
  require_relative 'outside_matrix/03_'
  require_relative 'outside_matrix/04_'
  require_relative 'outside_matrix/05_'
  require_relative 'outside_matrix/06_'
  require_relative 'outside_matrix/07_'
  require_relative 'outside_matrix/08_'
  require_relative 'outside_matrix/09_'
  require_relative 'outside_matrix/10_'
  require_relative 'outside_matrix/11_'
  require_relative 'outside_matrix/12_'
  require_relative 'outside_matrix/13_'
  require_relative 'outside_matrix/14_'
  require_relative 'outside_matrix/15_'
  require_relative 'outside_matrix/16_'
  require_relative 'outside_matrix/17_'
  require_relative 'outside_matrix/18_'
  require_relative 'outside_matrix/19_'
  require_relative 'outside_matrix/20_'
end

require_relative 'inside_docker/inside_docker' if are_running_in_a_docker?

require_relative 'smith_in_matrix/smith_in_matrix' if are_running_in_a_CI?

if are_running_in_a_vm?
  require_relative 'support/helpers'
  include Helpers
  require_relative 'inside_a_vm/inside_a_vm'
end
