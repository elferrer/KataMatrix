describe "Inside docker" do

  context "the Matrix" do
    it "exist" do
      in_the_matrix = matrix?

      expect(in_the_matrix).to be true
    end
  end

  context "User" do
    it "#{Config.env[:matrix_user_one]} exist" do
      user = system_user

      expect(user).to eq Config.env[:matrix_user_one]
    end
  end

end
