describe "Inside VM " do
  context "Matrix" do
    it "exist" do
      in_the_matrix = matrix?

      expect(in_the_matrix).to be true
    end
  end

  context "the machine" do
    make = [Config.env[:nodeClusterOne], Config.env[:nodeClusterTwo], Config.env[:nodeClusterThree]]

    make.each do |creation|
      it "#{creation} is at '/etc/hosts'" do
        the_hosts = "/etc/hosts"
        remote_machine = creation

        etcHosts_content = File.read(the_hosts)

        expect(etcHosts_content).to include remote_machine
      end
    end
  end

  context "#{Config.env[:matrix_user_one]} has ssh keys in virtual machine " do
    key = SpecConfig.env[:masterCluster_key]
    home = ENV['HOME']
    safe_box_in_vm = "#{home}/#{SpecConfig.env[:safe_box]}"
    before(:all) do
      @softkey = "#{safe_box_in_vm}/#{key}"
    end

    make = ['', '.pub']
    make.each do |creation|
      it "has a #{creation} key" do
        initial_location = Dir.pwd
        Dir.chdir(safe_box_in_vm)

        content = Dir.glob("*")
        Dir.chdir(initial_location)

        expect(content).to include key+creation
      end
    end

    it "has a protected directory" do
      private_dir = File.new(safe_box_in_vm)

      private_properties = private_dir.stat
      dir_properties = "%o" % private_properties.mode

      expect(dir_properties).to eq SpecConfig.env[:permissions_dir_700]
    end

    it "has protected the ssh #{SpecConfig.env[:masterCluster_key]} private key" do
      private_key = File.new(@softkey)

      key_properties = private_key.stat
      file_properties = "%o" % key_properties.mode

      expect(file_properties).to eq SpecConfig.env[:permissions_file_600]
    end

    it "has protected the ssh #{SpecConfig.env[:masterCluster_key]}.pub public key" do
      private_key = File.new("#{@softkey}.pub")

      key_properties = private_key.stat
      file_properties = "%o" % key_properties.mode

      expect(file_properties).to eq SpecConfig.env[:permissions_file_644]
    end
  end

  context "have a ssh port" do
    key = SpecConfig.env[:masterCluster_key]
    home = ENV['HOME']
    safe_box_in_vm = "#{home}/#{SpecConfig.env[:safe_box]}"
    before(:all) do
      @softkey = "#{safe_box_in_vm}/#{key}"
    end

    make = [Config.env[:nodeClusterOne], Config.env[:nodeClusterTwo], Config.env[:nodeClusterThree]]
    make.each do |remote_machine|
      it "open to #{remote_machine}" do
        remote_user = Config.env[:cluster_user]
        shell_sentence = "'echo 2>&1' && echo #{SpecConfig.env[:key_is_Ok]} || echo #{SpecConfig.env[:key_is_NOT_Ok]}"

        trying_open_door = launch_shellsentence_to_ssh_connection(@softkey,remote_user,remote_machine,shell_sentence)

        expect(trying_open_door).to include SpecConfig.env[:key_is_Ok]
      end
    end
  end

  context "have installed" do
    it "Nginx" do
      shell_sentence = system("nginx -v 2> /dev/null")

      expect(shell_sentence).to be true
    end

    context "and Nginx service is configured" do
      the_nginx_conf = "/etc/nginx/nginx.conf"
      nginx_conf = File.read(the_nginx_conf)

      make = [Config.env[:nodeClusterOne], Config.env[:nodeClusterTwo], Config.env[:nodeClusterThree]]
      make.each do |remote_machine|
        it "to integrate #{remote_machine}" do
          the_machine = remote_machine

          ip_of_the_machine = get_the_ip_from_hosts(the_machine)

          expect(nginx_conf).to include(ip_of_the_machine)
        end
      end
    end
  end

  context "have installed" do
    it "kubectl" do
      shell_sentence = `kubectl version 2>/dev/null`

      expect(shell_sentence).to include ("Client Version")
    end
  end

  context "have installed" do
    it "rke" do
      shell_sentence = system("rke --version > /dev/null")

      expect(shell_sentence).to be true
    end

    context "and rke service is configured" do
      the_cluster_yml = "#{ENV['HOME']}/cluster.yml"
      cluster_yml = File.read(the_cluster_yml)

      make = [Config.env[:nodeClusterOne], Config.env[:nodeClusterTwo], Config.env[:nodeClusterThree]]
      make.each do |remote_machine|
        it "to integrate #{remote_machine}" do
          the_machine = remote_machine

          ip_of_the_machine = get_the_ip_from_hosts(the_machine)

          expect(cluster_yml).to include(ip_of_the_machine)
        end
      end
    end
  end

  context "have configured" do
    it "kubectl" do
      kube_config = ENV['HOME'] + '/.kube/config'

      exist_kube_config = File.file?(kube_config)

      expect(exist_kube_config).to be true
    end
  end

  context "have installed" do
    it "helm" do
      shell_sentence = system("helm version > /dev/null")

      expect(shell_sentence).to be true
    end
  end

  context "service account tiller" do
    it "is created" do
      found_tiller = system("kubectl -n kube-system get deployment tiller-deploy > /dev/null")

      expect(found_tiller).to be true
    end
  end

  context "service cert-manager" do
    it "is created" do
      found_cert = system("kubectl -n kube-system get deployment cert-manager > /dev/null")

      expect(found_cert).to be true
    end
  end

  context "rancher deploy" do
    it "is successfully" do
      found_rancher = system("kubectl get pods --all-namespaces | grep -i rancher* > /dev/null")

      expect(found_rancher).to be true
    end
  end
end
