# KataMatrix (versión 0.2)

(Acerca de cómo está organizada la Kata.)


# ¿Quienes han intervenido en el proyecto?

Mira "extra/humans.txt".


# La Kata para intrépidos

Mira "extra/KATA.md".


# La Kata para pasear por Matrix, autoexplicativa

- KataMatrix101.es.md
- KataMatrix102.es.md
- KataMatrix103.es.md
- KataMatrix104.es.md
- KataMatrix105.es.md
- KataMatrix106.es.md
- KataMatrix107.es.md
- KataMatrix108.es.md
- KataMatrix109.es.md
- KataMatrix110.es.md
- KataMatrix111.es.md
- KataMatrix112.es.md
- KataMatrix113.es.md
- KataMatrix114.es.md
- KataMatrix115.es.md
- KataMatrix116.es.md
- KataMatrix117.es.md
- KataMatrix118.es.md


### Resumen

Una vez finalizada la KataMatrix y siguiendo el 'naming' y configuración propuesto en los 18 niveles; podemos lanzar los tests para todos los entornos con los comandos siguientes:

#### a) para un entorno no virtualizado

Todos los tests (155 tests):

~~~
rspec
~~~

o sin el test que hace un 'build' de docker:

~~~
rspec --tag ~docker
~~~


#### b) para el entorno docker

Primero debemos generar la imagen docker... si la habíamos borrado o alterado:

~~~
docker build --build-arg SOUL=Neo -t matrix .
~~~

Una vez tengamos la imagen docker generada, lanzamos los tests en el entorno docker (2 tests):

~~~
docker run -it matrix /bin/bash -c "rspec"
~~~


#### c) para el entorno con máquinas virtuales

Copiamos KataMatrix al entorno virtualizado:

~~~
rspec --tag upload_test_to_machine
~~~

Lanzamos los tests (26 tests):

~~~
ssh -i .ssh/neo rancher@neo-machine "cd connectionToMatrixInGuest && rspec"
~~~

Y limpiamos las conexiones y carpetas temporales creadas:

~~~
rspec --tag remove_tests_from_machine
~~~


#### d) entorno CI (gitlab-ci)

Hacemos 'push' contra nuestro repo y revisamos el pipeline ejecutado (1 test).


# Una pequeña chuleta sobre docker

Mira "extra/CHEATSHEET.md".


## Una pequeña utilidad para manejar docker

Archivo: "extra/managedocker.sh" (usar con mucha precaución)
