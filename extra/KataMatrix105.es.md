# KataMatrix 1x05: Quinto nivel

## En las profundidades de Matrix... ¡refactor!

Empezamos a tener demasiada información de variables repetidas por ahí, vamos a unificar el uso de las variables que estamos utilizando.

Para ello crearemos una clase que contenga las variables. Recuerda que tenemos los test en verde, ¡y este refactor terminará en verde!

Vamos a crear un nuevo archivo "matrix_config.rb":

~~~
class Config
  def self.env
    data = {
      :generic_name_of_a_docker_container => 'matrix',
      :matrix_user_one => "Neo",
    }
    return data
  end
end
~~~

Aprovechamos que vamos a hacer cambios y le añadimos un poquito de _mojito_ en el archivo "matrix.rb":

~~~
require './matrix_config'

def build_a_docker_image
  puts "    ·\033[33m(building docker image... it will take a while...)"
  system("docker build -t #{Config.env[:generic_name_of_a_docker_container]} . > /dev/null")
end

def load_docker_working_directory
  `docker inspect #{Config.env[:generic_name_of_a_docker_container]} | grep -m1 "WorkingDir" | cut -d '"' -f 4`
end

def are_running_in_a_docker?
  is_running_docker? && system_user == Config.env[:matrix_user_one]
end
~~~

Los cambios en el archivo "spec/inside_docker/inside_docker.rb":

~~~
  context "User" do
    it "#{Config.env[:matrix_user_one]} exist" do
      user = system_user

      expect(user).to eq Config.env[:matrix_user_one]
    end
  end
~~~

y añadimos un nuevo test a "spec/outside_matrix/03_.rb":

~~~
describe "3. The user" do
  it "is not '#{Config.env[:matrix_user_one]}' " do
    user = system_user

    expect(user).not_to eq Config.env[:matrix_user_one]
  end
end
~~~

y que no se nos olvide requerir el test en el archivo "spec/matrix_spec.rb"...

En realidad este test ya está en verde, pero nos sirve de apoyo a su homónimo en el entorno docker.
