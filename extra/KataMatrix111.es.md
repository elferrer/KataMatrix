# KataMatrix 1x11: Onceavo nivel

## La llave de Neo

Empecemos con los tests. Vamos a ver si Neo tiene una llave preparada.

~~~
describe "11. The ssh key of user '#{Config.env[:matrix_user_one]}'" do
  key = SpecConfig.env[:user_of_masterCluster_key]
  before(:all) do
    @softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
  end

  make = ['', '.pub']
  make.each do |creation|
    it "has a file #{creation} key" do
      Dir.chdir(SpecConfig.env[:safe_box])

      content = Dir.glob("*")
      Dir.chdir('..')

      expect(content).to include key+creation
    end
  end

  it "it's in a protected directory" do
    private_key = File.new(SpecConfig.env[:safe_box])

    key_properties = private_key.stat
    file_properties = "%o" % key_properties.mode

    expect(file_properties).to eq SpecConfig.env[:permissions_dir_700]
  end

  it "has protected their ssh #{@softkey} private key"do
    private_key = File.new(@softkey)

    key_properties = private_key.stat
    file_properties = "%o" % key_properties.mode

    expect(file_properties).to eq SpecConfig.env[:permissions_file_600]
  end

  it "has protected their ssh #{@softkey} public key" do
    private_key = File.new("#{@softkey}.pub")

    key_properties = private_key.stat
    file_properties = "%o" % key_properties.mode

    expect(file_properties).to eq SpecConfig.env[:permissions_file_644]
  end
end
~~~

Creamos la llave:

~~~
ssh-keygen -b 2048 -t rsa -f .ssh/neo -q -N ""
~~~

Y lanzamos los tests.


## Preparar la máquina para 'Neo'

Una hardkey para Neo

~~~
describe "12. The machine '#{Config.env[:masterCluster]}'" do
  virtual_network = Config.env[:name_virt_network]
  the_machine = Config.env[:masterCluster]
  key = SpecConfig.env[:user_of_masterCluster_key]
  key_maker = OutsideVirtualizationConfig.env[:machine_template_key]
  before(:all) do
    @softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
    @softkey_keymaker = "#{SpecConfig.env[:safe_box]}/#{key_maker}"
  end

  it "has a '#{Config.env[:masterCluster_disk]}' copy of original harddisk" do
    hardkey = "#{Config.env[:masterCluster_disk]}"
    Dir.chdir(Config.env[:garage])

    content = Dir.glob(hardkey)
    Dir.chdir('..')

    expect(content[0]).to eq hardkey
  end
end
~~~

Ahora copia la llave:

~~~
cp machines/hardkey.raw machines/neo_hardkey.raw
~~~

Lanza los tests.

Testeamos si existe la vm, añade al describe:

~~~
it "is created" do
  the_machine = Config.env[:masterCluster]

  expect(list_of_vm).to include the_machine
end
~~~

Crea un nuevo archivo llamada 'cook/neo-machine.yml' con el siguiente contenido:

~~~
---
- name: Create a neo-machine
  hosts: localhost
  vars_files:
    - ../config/matrix_config.yml

  tasks:

    - name: create neo-machine
      command: >
              virt-install --connect qemu:///system
                --name {{ neo_machine.name }}
                --memory {{ neo_machine.mem }}
                --vcpus {{ neo_machine.cpus }}
                --hvm
                --network network={{ neo_machine.virt_network }}
                --vnc
                --os-type {{ neo_machine.os_type }}
                --disk size=10,path=../machines/{{ neo_machine.sda }}
                --cdrom ../machines/{{ liveCD }}
                --keymap es
~~~

ejecútalo:

~~~
ansible-playbook cook/neo-machine.yml
~~~

Una vez esté la máquina levantada, , lanza los tests.

### Apunte adicional

Hemos añadido a la configuración de creación de la máquina la siguiente opción:

~~~
  --noautoconsole
~~~

Esta línea lo que hará es que cuando lancemos la creación de la máquina se nos libere la consola inmediatamente sin enseñarnos el proceso de levantamiento de la máquina virtual.


## Ahora nos queda la parte más delicada...

Recapitulemos: tenemos una máquina con la llave del 'Creador de llaves', evidentemente, dicha llave es genérica y será utilizada por el 'Creador de llaves' para crear tantas llaves como se le demanden... por tanto... es inseguro tener la llave del 'Creador de llaves' metida dentro de nuestra máquina.

Ahora vamos a preguntarle a la máquina si podemos abrirla con nuestra llave, añadimos al describe:

~~~
context "have a ssh port" do
  it "open to #{key}" do
    ip_address = get_ip_from(the_machine,virtual_network)
    remote_user = Config.env[:cluster_user]
    shell_sentence = "'echo 2>&1' && echo #{SpecConfig.env[:key_is_Ok]} || echo #{SpecConfig.env[:key_is_NOT_Ok]}"

    trying_open_door = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

    expect(trying_open_door).to include SpecConfig.env[:key_is_Ok]
  end
end
~~~

Volvamos a la VM; sigamos los siguientes pasos:

a. vamos a necesitar (la primera vez) volver a activar la consola de ubuntu:

~~~
#VM
sudo ros console enable ubuntu
~~~

Reinicia la máquina (imprescindible) y entra con Autologin.

b. vamos a necesitar (la primera vez) una contraseña:

~~~
#VM
sudo passwd rancher
~~~

c. Debemos crear el directorio .ssh en la VM:

~~~
#VM
mkdir ~/.ssh
~~~

d. Volvemos a nuestra consola y le mandamos nuestra llave (deberás saber primero la IP automática asignada):

~~~
cat .ssh/neo.pub | ssh -i .ssh/softkey rancher@ip.of.the.machine "xargs -I '{}' echo '{}' >> ~/.ssh/authorized_keys"
~~~

Siguiente paso. Vamos a hacer la comprobación opuesta, aunque sabemos que en el disco raw que teníamos preparado ya no teníamos la llave del 'Creador de llaves', queremos cerciorarnos de ello, para ello añadimos al context:

~~~
it "and close their to keymaker" do
  ip_address = get_ip_from(the_machine,virtual_network)
  remote_user = Config.env[:cluster_user]
  shell_sentence = "'echo 2>&1' && echo #{SpecConfig.env[:key_is_Ok]} || echo #{SpecConfig.env[:key_is_NOT_Ok]}"

  trying_open_door = launch_shellsentence_to_ssh_connection(@softkey_keymaker,remote_user,ip_address,shell_sentence)

  expect(trying_open_door).to include SpecConfig.env[:key_is_NOT_Ok]
end
~~~

El test habrá salido en verde, en caso contrario es una alegría ya que nos da la oportunidad de borrar la llave:

~~~
cat .ssh/softkey.pub | ssh -i .ssh/neo rancher@ip.of.the.machine "xargs -I '{}' sed -i 's#{}##g' ~/.ssh/authorized_keys"
~~~

Y finalmente, comprobamos que solo podremos acceder mediante clave ssh, añadimos al context:

~~~
it "secure, with only publickey (without root user login)" do
  ip_address = get_ip_from(the_machine,virtual_network)
  remote_user = Config.env[:cluster_user]
  permission_only_have_a_publickey = "Permission denied (publickey)"

  check_door = ask_permissions_of_ssh_connection(ip_address,remote_user)

  expect(check_door).to include permission_only_have_a_publickey
end
~~~

Pongámonos a ello:

~~~
#VM
sudo vi /etc/ssh/sshd_config
~~~

y cambia los valores siguientes para denegar la entrada con contraseña por ssh:

~~~
PermitRootLogin no
PasswordAuthentication no
ChallengeResponseAuthentication no
UsePAM no
~~~

Revísalo bien, puede que estén repetidas (vas a tener que pelearte con "vi"). Ahora deberíamos realizar un "restart" del servicio ssh en la máquina VM.

~~~
#VM
sudo service ssh restart
~~~

Vuelve a lanzar los test.


## Replicar la configuración de DNS en la nueva máquina

El nombre de la máquina existe en el hosts anfitrión, test:

~~~
describe "13. The name of machine '#{Config.env[:masterCluster]}' " do
  virtual_network = Config.env[:name_virt_network]
  the_machine = Config.env[:masterCluster]
  key = SpecConfig.env[:user_of_masterCluster_key]
  before(:all) do
    @softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
  end

    context "exist" do
      it "and it's in the system" do
        the_hosts = "/etc/hosts"
        ip_address = get_ip_from(the_machine,virtual_network)

        to_search = `cat #{the_hosts} | grep '#{the_machine}'`

        expect(to_search).to include ip_address
        expect(to_search).to include the_machine
      end
    end
end
~~~

Edita en tu máquina el archivo '/etc/hosts' y añade la siguiente línea:

~~~
ip.of.the.machine  neo-machine
~~~

Ahora comprobaremos que dentro de la VM también está identificado, añade al describe:

~~~
context "and it's" do
  before(:all) do
    @softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
  end

  it "on" do
    ip_address = get_ip_from(the_machine,virtual_network)
    the_hosts = "/etc/hosts"

    remote_user = Config.env[:cluster_user]
    shell_sentence = "'cat #{the_hosts}'"
    to_search = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

    expect(to_search).to include the_machine
  end
end
~~~

Bien, ahora viene el trabajo de campo. Debemos entrar en la VM:

~~~
ssh -i .ssh/neo rancher@ip.of.the.machine
~~~

Una vez dentro añadiremos la misma línea al mismo archivo en la VM. Edita a mano el archivo '/etc/hosts' e incluye la misma línea que en tu máquina:

~~~
ip.of.the.machine  neo-machine
~~~

Lanzamos los tests.


## Configuración de la red en la nueva máquina

Testeamos el nameserver:

~~~
describe "14. The machine '#{Config.env[:masterCluster]}' "  do
  virtual_network = Config.env[:name_virt_network]
  the_machine = Config.env[:masterCluster]
  key = SpecConfig.env[:user_of_masterCluster_key]
  before(:all) do
    @softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
  end

  context "have" do
    it "a dns nameservers" do
      ip_address = get_ip_from(the_machine,virtual_network)
      expected_nameservers = "- 8.8.8.8\n- 8.8.4.4\n\n"

      remote_user = Config.env[:cluster_user]
      shell_sentence = "'sudo ros config get rancher.network.dns.nameservers'"
      ask_nameservers = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

      expect(ask_nameservers).to eq expected_nameservers
    end
  end
end
~~~

Y en la vm configuramos los 'nameservers'.

~~~
#VM
sudo ros config set rancher.network.dns.nameservers [8.8.8.8,8.8.4.4]
~~~

Testeamos el gateway, lo añadimos al context:

~~~
it "a valid gateway" do
  ip_address = get_ip_from(the_machine,virtual_network)
  remote_user = Config.env[:cluster_user]

  shell_sentence = "\"/sbin/ip route | awk '/#{Config.env[:name_virt_network]}/ { print $3 }'\""
  ask_gateway = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)
  expected_gateway = ask_gateway.match(/#{Config.env[:ip_expression]}/).to_s
  expected_gateway = "Machine off" if expected_gateway == ""
  shell_sentence = "'sudo ros config get rancher.network.interfaces.eth0.gateway'"
  configured_gateway = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

  expect(configured_gateway).to include expected_gateway
end
~~~

Configura el gateway correcto (puedes consultarlo en la máquina virtual lanzando un 'route -n'):

~~~
#VM
sudo ros config set rancher.network.interfaces.eth0.gateway ip.of.the.gateway
~~~

Testeamos si la IP es válida:

~~~
it "a valid address ip" do
  ip_address = get_ip_from(the_machine,virtual_network)
  ip_range = "#{ip_address}/24\n"

  remote_user = Config.env[:cluster_user]
  shell_sentence = "'sudo ros config get rancher.network.interfaces.eth0.address'"
  configured_ip = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

  expect(configured_ip).to eq ip_range
end
~~~

la configuración de la IP:

~~~
#VM
sudo ros config set rancher.network.interfaces.eth0.dhcp false
sudo ros config set rancher.network.interfaces.eth0.address ip.of.the.machine/24
~~~

Aunque si hemos llegado a este punto es porque todo se ha resuelto satisfactoriamente, haremos una validación en la VM de que está correctamente configurado:

~~~
#VM
sudo ros config validate -i /var/lib/rancher/conf/cloud-config.yml
~~~

Lanzamos tests.

Comprobamos la versión de docker:

~~~
it "a correct docker engine" do
  ip_address = get_ip_from(the_machine,virtual_network)
  docker_engine = "current  docker-18.06.3-ce\n"

  remote_user = Config.env[:cluster_user]
  shell_sentence = "\"sudo ros engine list | grep 'current'\""
  configured_engine = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

  expect(configured_engine).to eq docker_engine
end
~~~

Y si es necesario actualizamos el motor:

~~~
sudo ros engine switch docker-18.06.3-ce
sudo shutdown -r now
~~~

Volvemos a lanzar los tests.
