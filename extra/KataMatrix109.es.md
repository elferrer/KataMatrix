# KataMatrix 1x09: Noveno nivel

## Refactor

Refactor: Vamos a necesitar lanzar los test en diferentes condiciones, bien cuando la máquina "keysring-of-keymaker" esté encendida (running), bien cuando ésta esté apagada (shut off). ¿Porqué? Por que no queremos depender de poder testear la situación si alguna de las máquinas no está disponible, por tanto, debemos tener la previsión de qué testar y en qué condiciones.

Apaga por ssh (te pongo una ip de ejemplo):

~~~
ssh -i .ssh/softkey rancher@ip.of.the.machine 'sudo shutdown -h now'
~~~

De momento, haz el refactor necesario, y ya sabes, apaga/enciende la VM y comprueba que los test pasen en verde.

Enciende la VM a través de los comandos de la librería libvirt:

~~~
virsh start keysring-of-keymaker
~~~

Lanza los tests. Vuelve a apagar la máquina:

~~~
virsh shutdown keysring-of-keymaker
~~~

Y si no lo consigues... aquí tienes el refactor:

~~~
describe "8. The machine  '#{Config.env[:machine_template]}' " do
  virtual_network = Config.env[:name_virt_network]
  the_machine = Config.env[:machine_template]
  key = OutsideVirtualizationConfig.env[:machine_template_key]
  before(:all) do
    @softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
  end

  case
  when list_of_shutoff_vm.include?(the_machine)
    it "·\033[33m(is shut off)\033[0m" do

      expect(list_of_shutoff_vm).to include OutsideVirtualizationConfig.env[:VM_is_off]
    end

  when list_of_running_vm.include?(the_machine)
    it "is running" do
      ip_address = get_ip_from(the_machine,virtual_network)

      expect(list_of_running_vm).to include OutsideVirtualizationConfig.env[:VM_is_running]
    end

    context "exist" do
      it "and only reboot if necessary" do
        ip_address = get_ip_from(the_machine,virtual_network)

        reboot_vm_and_return_a_message(:failure_in_name_resolution,the_machine,virtual_network) if ip_address == :failure_in_name_resolution
      end

      it "and only trying to reconnect ssh if necessary" do
        remote_user = Config.env[:cluster_user]
        ip_of_the_machine = get_ip_from(the_machine,virtual_network)

        trying_open_door = test_ssh_connection_to_vm(ip_of_the_machine,@softkey,remote_user)

        reconnect_to_vm_and_return_a_message("I have lost SSH connection",the_machine,virtual_network,@softkey,remote_user) unless trying_open_door.include?(SpecConfig.env[:key_is_Ok])
      end

      it "and their connection is alive" do
        ip_address = get_ip_from(the_machine,virtual_network)

        expect(ip_address).to_not equal :failure_in_name_resolution
      end
    end

    context "have a ssh port" do
      it "open to #{key}" do
        ip_address = get_ip_from(the_machine,virtual_network)
        remote_user = Config.env[:cluster_user]
        shell_sentence = "'echo 2>&1' && echo #{SpecConfig.env[:key_is_Ok]} || echo #{SpecConfig.env[:key_is_NOT_Ok]}"

        trying_open_door = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

        expect(trying_open_door).to include SpecConfig.env[:key_is_Ok]
      end

      it "secured, with only publickey (without root user login)" do
        ip_address = get_ip_from(the_machine,virtual_network)
        remote_user = Config.env[:cluster_user]
        permission_only_have_a_publickey = "Permission denied (publickey)"

        check_door = ask_permissions_of_ssh_connection(ip_address,remote_user)

        expect(check_door).to include permission_only_have_a_publickey
      end
    end
  end
end
~~~

... y algún método que puede faltarte en "matrix.rb":

~~~
def test_ssh_connection_to_vm(the_mac_address,key,remote_user)
  the_mac_address = the_mac_address.to_s
  remote_user = Config.env[:cluster_user]
  shell_sentence = "'echo 2>&1' && echo #{SpecConfig.env[:key_is_Ok]} || echo #{SpecConfig.env[:key_is_NOT_Ok]}"
  connection_availability = launch_shellsentence_to_ssh_connection(key,remote_user,the_mac_address,shell_sentence)
  return connection_availability
end
~~~
