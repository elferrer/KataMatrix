# KataMatrix 1x18: Dieciochoavo nivel

## Configurar Kubectl y lanzar Rancher

Lo primero que debemos hacer es configurar el "Tiller":

~~~
context "service account tiller" do
  it "is created" do
    found_tiller = system("kubectl -n kube-system get deployment tiller-deploy > /dev/null")

    expect(found_tiller).to be true
  end
end
~~~

Básicamente repasaremos la configuración y la sobreescribiremos a nuestro gusto, ya que "Helm" ya había creado "Tiller". Posteriormente haremos el deploy:

~~~
kubectl -n kube-system create serviceaccount tiller
kubectl create clusterrolebinding tiller  --clusterrole cluster-admin  --serviceaccount=kube-system:tiller
helm init --service-account tiller --upgrade
kubectl -n kube-system rollout status deploy/tiller-deploy
~~~

El test está en verde, realmente ya estaba en verde antes de esta configuración. Dejamos en tus manos la comprobación que verifique estos cambios respecto a los iniciales.

Ahora vamos a testear la creación del servicio "cert-manager":

~~~
context "service cert-manager" do
  it "is created" do
    found_cert = system("kubectl -n kube-system get deployment cert-manager > /dev/null")

    expect(found_cert).to be true
  end
end
~~~

Ahora vamos a añadir el repo de Rancher a Helm, y a continuación crearemos los certificados e instalaremos "cert-manager" con Helm:

~~~
helm repo add rancher-stable https://releases.rancher.com/server-charts/stable
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/rancher-selfsigned.key -out /etc/ssl/certs/rancher-selfsigned.crt
sudo openssl req -out /etc/ssl/ca.pem -new -x509
helm install stable/cert-manager   --name cert-manager   --namespace kube-system  --version v0.5.2
~~~

Lanzamos los tests y comprobamos que todo está en orden.

Y finalmente, después de tanto tiempo, vamos a escribir el último test. Vamos a verificar que Rancher está desplegado:

~~~
context "rancher deploy" do
  it "is successfully" do
    found_rancher = system("kubectl get pods --all-namespaces | grep -i rancher* > /dev/null")

    expect(found_rancher).to be true
  end
end
~~~

Y pasamos a desplegarlo. Creamos el "namespace" necesario, instalamos Rancher con Helm y lo desplegamos con Kubectl:

~~~
kubectl create namespace cattle-system
helm install rancher-stable/rancher --name rancher --namespace cattle-system --set hostname=neo-machine
kubectl -n cattle-system rollout status deploy/rancher
~~~

Dale un tiempecito a que haga el despliegue. Después lanza los tests.

Ahora solo queda conectar por web a tu cluster HA de Rancher, en nuestro caso: http://neo-machine

Si no has comprado certificados y te los has hecho tu mismo tal como hemos seguido en la Kata, entonces, tendrás un aviso de un certificado no válido... la causa es que el creador y el utilizador son el mismo, por lo que el receptor (visitante de la web) debe ser informado de ello.


## Ejercicio

Testea la instalación de un Grafana en Rancher.


## Fin
