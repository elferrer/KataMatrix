# KataMatrix 1x13: Treceavo nivel

En este nivel tendremos que prepararnos para poder compartir archivos entre el equipo anfitrión y la máquina virtual. Como ya has visto anteriormente con docker es bastante sencillo. Sin embargo no tenemos dicha facilidad para compartir archivos entre máquinas.

Pero tenemos más potencia, tenemos todas las opciones que nos brinda nuestro sistema operativo. Así pues, optaremos por una de las más seguras: 'sshfs'.


## Prepara el sistema para compartir directorio

Lo primero será testear que tenemos las herramientas necesarias instaladas en el sistema, en nuestro caso optamos por 'sshfs':

~~~
describe "18. Host system" do
  it "have 'sshfs' connector" do
    program = 'sshfs'

    found_program = system("which #{program} > /dev/null")

    expect(found_program).to be true
  end
end
~~~

Ahora podemos instalarnos 'sshfs' en la máquina (anfitrión):

~~~
sudo apt install sshfs
~~~

Ahora testearemos la configuración necesaria, añade al describe:

~~~
it "it's configured to allow 'sshfs' connections" do
  user_allow_other = "user_allow_other\n"

  search_uncommented_text = `cat /etc/fuse.conf | grep "^user_allow_other"`

  expect(search_uncommented_text).to eq user_allow_other
end
~~~

Y editamos la configuración:

~~~
sudo nano /etc/fuse.conf
~~~

Descomentamos la línea:

~~~
user_allow_other
~~~

y lanzamos los tests.


### Ahora un breve apunte.

Vamos a explicar la siguiente línea de conexión (no la lances, después volveremos a la kata):

~~~
sshfs -o allow_other,IdentityFile=$PWD/.ssh/neo rancher@neo-machine:connectionToMatrixInGuest $PWD/connectionToMatrixInHost
~~~

¿Qué son esas letricas?:

_allow_other_ permite la conexión de usuarios que no sean root.

_IdentityFile_ debe tener la ruta completa hasta la llave, no acepta rutas relativas.

_rancher@neo-machine:connectionToMatrixInGuest_ es la conexión a la máquina remota, cuyo punto de montaje está a partir del home del usuario, no acepta rutas fuera del espacio del usuario.

_$PWD/connectionToMatrixInHost_ y finalmente, la carpeta en el espacio del usuario del host, permite rutas relativas o absolutas y deben estar vacías, de lo contrario hay que especificarlo como parámetro. Esta medida de protección es para evitar la pérdida de datos.

y, cuando terminemos de utilizar la conexión, teniendo en cuenta que estamos en la ruta que cuando hemos conectado, podemos desmontarla con:

~~~
sudo umount /path/to/mount/connectionToMatrixInHost
~~~

Una breve anotación: también podemos hacernos una conexión perdurable ante reinicios, para ello solo tienes que editar el archivo '/etc/fstab' y añadir al final una línea (con las mismas características que antes) del estilo:

~~~
sshfs#user@machine:connectionToMatrixInGuest /path/to/mount/connectionToMatrixInHost
~~~


### Volvamos a la kata.

Es hora de repensar en qué situación estamos.

Ya tenemos un test que nos comprueba que tenemos configurado 'sshfs', también hemos visto en el apunte anterior cómo se comparte una carpeta a través de una conexión segura con 'sshfs'.

El siguiente punto sería formalizar en un test la creación real de la conexión y posteriormente el acceso a través de la máquina remota a la carpeta indicada.

En primer lugar, tenemos que decidirnos si hacer una conexión perdurable o no. En la kata no vamos a montar las conexiones en '/etc/fstab'. Lo que haremos es mantener un entorno sano preparando la conexión en aquellos tests que la necesiten.


## Copiar la kata a la VM

Vamos a ha testear que es posible crear una carpeta temporal.

¿Qué sentido tiene testear la creación de una carpeta temporal? Tener la certeza que tienes acceso y permisos para su creación.

Con el siguiente test creamos la carpeta y comprobamos que se ha creado:

~~~
describe "19. Connection to virtual machine '#{Config.env[:masterCluster]}'" do
  virtual_network = Config.env[:name_virt_network]
  the_machine = Config.env[:masterCluster]
  key = SpecConfig.env[:user_of_masterCluster_key]
  before(:all) do
    @softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
  end

  context "from side of host" do
    it "it's possible by creating a new folder" , :upload_test_to_machine => true do
      initial_location = Dir.pwd

      three = Config.env[:folderAtHost].split("/")
      three.each do |directory|
        Dir.chdir("/") if directory == ""
        next if directory == ""
        Dir.mkdir(directory) unless Dir.exist?(directory)
        Dir.chdir(directory)
      end
      Dir.chdir(initial_location)
      check_directory = File.directory?(Config.env[:folderAtHost])

      expect(check_directory).to be true
    end
  end
end
~~~

En nuestro caso puedes comprobarlo haciendo un:

~~~
ls /tmp/connectionToMatrixInHost
~~~

Test de creación en la VM:

~~~
context "in side of guest" do
  it "it's possible by creating a new folder" , :upload_test_to_machine => true  do
    the_machine = Config.env[:masterCluster]
    key = SpecConfig.env[:user_of_masterCluster_key]
    softkey = "#{SpecConfig.env[:safe_box]}/#{key}"

    ip_address = get_ip_from(the_machine,virtual_network)
    remote_user = Config.env[:cluster_user]
    shell_sentence = "mkdir -p #{Config.env[:folderAtGuest]}"
    trying_open_door = launch_shellsentence_to_ssh_connection(softkey,remote_user,ip_address,shell_sentence)
    shell_sentence = "ls"
    content_directory = launch_shellsentence_to_ssh_connection(softkey,remote_user,ip_address,shell_sentence)

    expect(content_directory).to include Config.env[:folderAtGuest]
  end
end
~~~

Entramos en la máquina y comprobamos que la carpeta existe. También podemos verlo lanzando un comando a través de ssh:

~~~
ssh -i .ssh/neo rancher@neo-machine 'ls connectionToMatrixInGuest'
~~~

Ahora vamos a comprobar que es posible montar la carpeta del host contra la carpeta del guest:

~~~
it "and connection is possible" , :upload_test_to_machine => true do
  groupOwnedBeforeLink_currentDir_fromHostFolder = File.grpowned?(Config.env[:folderAtHost])
  groupOwnedBeforeLink_parentDir_fromHostFolder = File.grpowned?(Config.env[:folderAtHost])
  remote_user = Config.env[:cluster_user]
  the_machine = Config.env[:masterCluster]
  key = SpecConfig.env[:user_of_masterCluster_key]
  softkey = "#{SpecConfig.env[:safe_box]}/#{key}"

  mount_folder_with_sshfs = `sshfs -o allow_other,IdentityFile=$PWD/#{softkey} #{remote_user}@#{the_machine}:#{Config.env[:folderAtGuest]} #{Config.env[:folderAtHost]}`
  groupOwnedAfterLink_currentDir_fromHostFolder = File.grpowned?(Config.env[:folderAtHost])
  groupOwnedAfterLink_parentDir_fromHostFolder = File.grpowned?(Config.env[:folderAtHost])

  expect(groupOwnedBeforeLink_currentDir_fromHostFolder).not_to eq groupOwnedAfterLink_currentDir_fromHostFolder
  expect(groupOwnedBeforeLink_parentDir_fromHostFolder).not_to eq groupOwnedAfterLink_parentDir_fromHostFolder
end
~~~

Pasemos al siguiente test. Ahora vamos a realizar el copiado de los archivos necesarios para poder testear desde dentro de la VM.

~~~
it "and copy tests to VM is possible" , :upload_test_to_machine => true do
  initial_location = Dir.pwd
  copy_list = [ 'Gemfile', 'matrix.rb', '.rspec', 'matrix_config.rb', 'config', 'spec', 'config_files']
  origin_object = initial_location
  destination_object = Config.env[:folderAtHost]

  copy_go_through_a_list(copy_list,origin_object,destination_object)
  Dir.chdir(initial_location)
  remote_user = Config.env[:cluster_user]
  the_machine = Config.env[:masterCluster]
  key = SpecConfig.env[:user_of_masterCluster_key]
  softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
  ip_address = get_ip_from(the_machine,virtual_network)
  shell_sentence = "ls #{Config.env[:folderAtGuest]} -la"
  content_directory = launch_shellsentence_to_ssh_connection(softkey,remote_user,ip_address,shell_sentence)

  copy_list.each do |object|
    expect(content_directory).to include(object)
  end
end
~~~

Vamos a necesitar crear las nuevas funciones "copy_go_through_a_list", "copy_file", "copy_directory" y "create_new_directory" en "matrix.rb":

~~~
def copy_go_through_a_list(objects_list,new_origin_object,destination_object)
  objects_list.each do |object|
    origin_object = [new_origin_object,object].join('/')
    new_destination = [destination_object,object].join('/')
    (copy_file(origin_object,new_destination) ; next) if File.file?(object)
    current_directory = Dir.pwd
    copy_directory(origin_object,new_destination) if File.directory?(object)
    Dir.chdir(current_directory)
  end
end

def copy_file(origin_object,destination_object)
  File.write(destination_object, File.read(origin_object))
end

def copy_directory(origin_object,destination_object)
  create_new_directory(destination_object)
  Dir.chdir(origin_object)
  new_origin_object = Dir.pwd
  objects_list = Dir.glob('*')
  copy_go_through_a_list(objects_list,new_origin_object,destination_object)
end

def create_new_directory(destination_object)
  Dir.mkdir(destination_object) unless Dir.exist?(destination_object)
end
~~~

Ahora testemos que podemos borrar la carpeta en la VM para limpiar el entorno:

~~~
it "and remove guest folder to clean environment is possible" , :remove_tests_from_machine => true  do
  the_machine = Config.env[:masterCluster]
  key = SpecConfig.env[:user_of_masterCluster_key]
  softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
  ip_address = get_ip_from(the_machine,virtual_network)
  remote_user = Config.env[:cluster_user]
  shell_sentence = "sudo rm -r #{Config.env[:folderAtGuest]}"

  remove_folder = launch_shellsentence_to_ssh_connection(softkey,remote_user,ip_address,shell_sentence)
  shell_sentence = "ls -la"
  ls_la = launch_shellsentence_to_ssh_connection(softkey,remote_user,ip_address,shell_sentence)

  expect(remove_folder).to_not include(Config.env[:folderAtGuest])
end
~~~

Vamos a necesitar crear la nueva función "remove_directory" en "matrix.rb":

~~~
def remove_directory(directory)
  if File.directory?(directory)
    Dir.foreach(directory) do |object|
      remove_directory("#{directory}/#{object}") if ((object.to_s != ".") and (object.to_s != ".."))
    end
    Dir.delete(directory)
  else
    File.delete(directory)
  end
end
~~~

El siguiente paso es testear que podemos desmontar la conexión:

~~~
it "and unmount connection is possible" , :remove_tests_from_machine => true do
  `fusermount -u #{Config.env[:folderAtHost]}`
  check_unmount = `fuser -M #{Config.env[:folderAtHost]} 2>&1`

  expect(check_unmount).to include('is not a mountpoint')
end
~~~

Y como punto final borramos la carpeta temporal que habíamos creado en el host:

~~~
context "it's cleared at host" do
  it "by removing folder" , :remove_tests_from_machine => true do

    remove_directory(Config.env[:folderAtHost])
    error = "No such file or directory @ dir_s_chdir - #{Config.env[:folderAtHost]}"

    expect{Dir.chdir(Config.env[:folderAtHost])}.to raise_error(error)
  end
end
~~~


### Trucos

Como habrás visto en los test hemos puesto un par de tags.

Como hemos optado por utilizar una carpeta temporal a la que vamos copiando los archivos que queremos conectar con la máquina virtual, esto nos ha obligado a tener que actualizar a mano la carpeta temporal. Para agilizar las pruebas de cambio en el código podemos lanzar los siguientes tags:

Para borrar la carpeta temporal y la conexión:

~~~
rspec --tag remove_tests_from_machine
~~~

Para volver a crear la carpeta temporal y la connexión:

~~~
rspec --tag upload_test_to_machine
~~~

Evidentemente tu puedes haber optado por no utilizar una carpeta temporal y mantener siempre viva la conexión a través de 'fstab'.
