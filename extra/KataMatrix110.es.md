# KataMatrix 1x10: Décimo nivel

Como ya sabes, la IP es el identificador en la red de cada máquina, y si queremos poder mantener su IP y/o poder localizarla a través de un nombre, hemos de asignarle una IP fija y un nombre en el DNS (Domain Name System).

Para la kata nos va a servir que nuestro sistema haga las veces de DNS, por lo que solo tendremos que dar de alta la IP en el archivo '/etc/hosts'. Crearemos primero el test:

~~~
describe "9. The name of machine '#{Config.env[:machine_template]}' " do
  virtual_network = Config.env[:name_virt_network]
  the_machine = Config.env[:machine_template]
  key = OutsideVirtualizationConfig.env[:machine_template_key]

  before(:all) do
    @softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
  end

  context "exist" do
    it "and is at the system" do
      the_hosts = "/etc/hosts"
      ip_address = get_ip_from(the_machine,virtual_network)

      to_search = `cat #{the_hosts} | grep '#{the_machine}'`

      expect(to_search).to include ip_address
      expect(to_search).to include the_machine
    end
  end

end
~~~

Para seguir con la estructura que hemos creado deberás guardar este test como 'spec/outside_matrix/09_.rb'. Además deberás requerirlo en 'spec/matrix_spec.rb'.

Edita en tu máquina el archivo '/etc/hosts' y añade la siguiente línea (indicando la IP de la máquina virtual):

~~~
ip.of.the.machine  keysring-of-keymaker
~~~

Lanza los tests.

Ahora hay que hacer lo mismo en la VM, comprobaremos que dentro de la VM existe en el archivo '/etc/hosts' el nombre e IP de la misma máquina:

~~~
context "and the '#{Config.env[:machine_template]}' machine" do
  it "have their name in their DNS" do
    the_hosts = "/etc/hosts"

    ip_address = get_ip_from(the_machine,virtual_network)
    remote_user = Config.env[:cluster_user]
    shell_sentence = "'cat #{the_hosts}'"
    to_search = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

    expect(to_search).to include the_machine
  end
end
~~~

Bien, ahora viene el trabajo de campo. Debemos entrar en la VM:

~~~
ssh -i .ssh/softkey rancher@ip.of.the.machine
~~~

Una vez dentro añadiremos, edita a mano el archivo '/etc/hosts' e incluye la misma línea que en tu máquina:

~~~
ip.of.the.machine  keysring-of-keymaker
~~~

¿Test en verde? Bien, ahora refactoriza los test para que sean sensibles al estado de la máquina... puedes basarte en el refactor que hicimos en el nivel anterior.


## Configurar el sistema virtual

Ahora toca configurar el sistema de navegación de nuestra VM, para ello tendremos que indicarle el 'dhcp', 'gateway', 'ip address' y 'dns nameservers'.

Empecemos por los tests para configurar los nameservers. Hemos de tener en cuenta que dado que estamos trabajando con RancherOS deberemos atenernos a ciertas particularidades de esta distro.

Esta parte la crearemos en un nuevo archivo 'spec/outside_matrix/10_.rb'. Además deberás requerirlo en 'spec/matrix_spec.rb'.

~~~
describe "10. The machine '#{Config.env[:machine_template]}' "  do
  virtual_network = Config.env[:name_virt_network]
  the_machine = Config.env[:machine_template]
  key = OutsideVirtualizationConfig.env[:machine_template_key]
  before(:all) do
    @softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
  end

  context "and it have" do
    it "a dns nameservers" do
      ip_address = get_ip_from(the_machine,virtual_network)
      expected_nameservers = "- 8.8.8.8\n- 8.8.4.4\n\n"

      remote_user = Config.env[:cluster_user]
      shell_sentence = "'sudo ros config get rancher.network.dns.nameservers'"
      ask_nameservers = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

      expect(ask_nameservers).to eq expected_nameservers
    end
  end
end
~~~

Y en la vm configuramos los 'nameservers'.

~~~
#VM
sudo ros config set rancher.network.dns.nameservers [8.8.8.8,8.8.4.4]
~~~

Lanzamos los tests y continuamos.

Ahora necesitamos comprobar que el gateway esté correctamente configurado:

~~~
it "a valid gateway" do
  ip_address = get_ip_from(the_machine,virtual_network)
  remote_user = Config.env[:cluster_user]

  shell_sentence = "\"/sbin/ip route | awk '/#{Config.env[:name_virt_network]}/ { print $3 }'\""
  ask_gateway = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)
  expected_gateway = ask_gateway.match(/#{Config.env[:ip_expression]}/).to_s
  expected_gateway = "Machine off" if expected_gateway == ""
  shell_sentence = "'sudo ros config get rancher.network.interfaces.eth0.gateway'"
  configured_gateway = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

  expect(configured_gateway).to include expected_gateway
end
~~~

Configura el gateway correcto (puedes consultarlo en la máquina virtual lanzando un 'route -n'):

~~~
#VM
sudo ros config set rancher.network.interfaces.eth0.gateway ip.of.the.gateway
~~~

Lanza el test para comprobar que es correcto.

Vamos a testear que tenemos una IP válida:

~~~
it "a valid address ip" do
  ip_address = get_ip_from(the_machine,virtual_network)
  ip_range = "#{ip_address}/24\n"

  remote_user = Config.env[:cluster_user]
  shell_sentence = "'sudo ros config get rancher.network.interfaces.eth0.address'"
  configured_ip = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

  expect(configured_ip).to eq ip_range
end
~~~

y después de lanzar los test lo configuramos:

~~~
#VM
sudo ros config set rancher.network.interfaces.eth0.dhcp false
sudo ros config set rancher.network.interfaces.eth0.address ip.of.the.machine/24
~~~

Aunque si hemos llegado a este punto es porque todo se ha resuelto satisfactoriamente, haremos una validación en la VM de que está correctamente configurado.

Los test que hemos hecho nos aseguran que todo está correcamente configurado, sin embargo, nunca está de más realizar las comprobaciones necesarias:

~~~
#VM
sudo ros config validate -i /var/lib/rancher/conf/cloud-config.yml
~~~

Si tras lanzar este comando hubiéramos recibido un error ello significa que nuestros tests no son lo suficientemente fuertes y habría que observar el porqué y solucionarlo.

Y por último, vamos a necesitar tener toda la potencia, así que revisemos el motor docker...

~~~
it "a correct docker engine" do
  ip_address = get_ip_from(the_machine,virtual_network)
  docker_engine = "current  docker-18.06.3-ce\n"

  remote_user = Config.env[:cluster_user]
  shell_sentence = "\"sudo ros engine list | grep 'current'\""
  configured_engine = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

  expect(configured_engine).to eq docker_engine
end
~~~

Lanzamos tests, y si es necesario actualizamos el motor:

~~~
sudo ros engine switch docker-18.06.3-ce
sudo shutdown -r now
~~~

Una vez reiniciada la máquina volvemos a lanzar los tests.

Refactoriza los tests para que sean sensibles ante fallos de la máquina.
