# KataMatrix 1x07: Séptimo nivel

A partir de este nivel las cosas se van a complicar, por lo merece la pena una introducción para entender en su globalidad lo que vamos a hacer:

a) Vamos a crear un Clúster HA con "RancherOS", en el cual tendremos funcionando "Rancher". El clúster consta de una máquina máster que hace de balanceador y tres máquinas nodos que siempre estarán activas y sincronizadas de tal forma que si una de las máquinas cae cualquiera de las otras dos podrá recoger el testigo mientras ésta se recupera automáticamente o en caso de gran desastre mientras nosotros rehacemos otro nodo y lo inyectamos en el clúster.

b) Rancher tiene una "ventana web" que nos permite trabajar de forma cómoda con Kubernetes... entre otras cosas.

c) Previamente nos crearemos una máquina que utilizaremos para clonar rápidamente la instalación en las otras máquinas. Esta máquina será la del "Creador de llaves". Su única razón de ser es agilizar nuestro trabajo. Esta máquina es prescindible aunque veremos que es una buena ayuda para iniciarnos.

d) Vamos a tener que trabajar a través de "ssh". Vamos a intentar automatizar los tests todo lo posible para no repetir cuatro veces los mismos tests.

e) La creación de las máquinas la automatizaremos con "ansible" lo que nos dará la oportunidad de compartir las mismas variables entre éste y Ruby.

f) Hemos detectado un pequeño problema con la red virtual de las máquinas, la red virtual desaparece y deja incomunicados los equipos al cabo de cierto tiempo... pueden ser 10/20 minutos u horas de vida. Prepararemos los tests para detectar si esto sucede y rehabilitar el sistema. En las primera pruebas que hicimos las máquinas virtuales no eran capaces de volver a conectarse a una nueva red creada posiblemente debido a que aunque mantuviera el nombre su identificador interno había cambiado, por lo que finalmente la solución era el reinicio de la máquina para que se volviera a enganchar. Si una vez terminada la KataMatrix encontramos la solución perfecta añadiremos otros tests que hagan posible dicha tarea. Una posible solución pendiente de estudio es mantener las conexiones "ssh" abiertas, pero, no me gusta este "hack" ya que nos obliga a tener un estado predefinido.


## Crear la llave del 'Creador de llaves'

Lo primero que haremos es comprobar si tenemos creada un par de llaves público/privada para las conexiones "ssh". Además, es imprescindible que las llaves estén protegidas para que no puedan ser manipuladas (y así garantizar su integridad que es la nuestra). Vayamos a por el test, archivo "spec/outside_matrix/06_.rb":

~~~
describe "6. The 'keymaker' user" do
  key = OutsideVirtualizationConfig.env[:machine_template_key]
  before(:all) do
    @softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
  end

  make = ['', '.pub']
  make.each do |creation|
    it "has a #{creation} key" do
      Dir.chdir(SpecConfig.env[:safe_box])

      content = Dir.glob("*")
      Dir.chdir('..')

      expect(content).to include key+creation
    end
  end

  it "has protected their directory" do
    private_dir = File.new(SpecConfig.env[:safe_box])

    private_properties = private_dir.stat
    dir_properties = "%o" % private_properties.mode

    expect(dir_properties).to eq SpecConfig.env[:permissions_dir_700]
  end

  it "has protected their private key '#{OutsideVirtualizationConfig.env[:machine_template_key]}' "do
    private_key = File.new(@softkey)

    key_properties = private_key.stat
    file_properties = "%o" % key_properties.mode

    expect(file_properties).to eq SpecConfig.env[:permissions_file_600]
  end

  it "has protected their the public key '#{OutsideVirtualizationConfig.env[:machine_template_key]}.pub' " do
    private_key = File.new("#{@softkey}.pub")

    key_properties = private_key.stat
    file_properties = "%o" % key_properties.mode

    expect(file_properties).to eq SpecConfig.env[:permissions_file_644]
  end
end
~~~

Vamos a poner como está quedando el archivo "spec/matrix_spec.rb":

~~~
require_relative '../matrix'
require './spec/matrix_spec_config'

if are_running_outside_the_matrix?
  require './spec/outside_matrix/outside_matrix_config'
  require_relative 'outside_matrix/01_'
  require_relative 'outside_matrix/02_'
  require_relative 'outside_matrix/03_'
  require_relative 'outside_matrix/04_'
  require_relative 'outside_matrix/05_'
  require_relative 'outside_matrix/06_'
end

require_relative 'inside_docker/inside_docker' if are_running_in_a_docker?

require_relative 'smith_in_matrix/smith_in_matrix' if are_running_in_a_CI?
~~~

Vemos que hemos añadido el archivo "spec/matrix_spec_config":

~~~
class SpecConfig
  def self.env
    data = {
      :safe_box => ".ssh",

      :permissions_dir_700 => "40700",
      :permissions_file_600 => "100600",
      :permissions_file_644 => "100644"
    }
    return data
  end
end
~~~

Lanzamos los tests.

Ahora, si quieres, podemos crear el par de llaves:

~~~
mkdir .ssh
chmod 700 .ssh
cd .ssh
ssh-keygen -b 2048 -t rsa -f softkey -q -N ""
chmod 600 softkey
chmod 644 softkey.pub
cd ..
~~~

Y lanzamos los tests. ¿Verde?

Antes de continuar y sabiendo que no debes subir las llaves a ninguna parte (ni siquiera dentro de una imagen docker) añadiremos al '.dockerignore' la carpeta entera (añadimos otras carpetas y archivos sin pararnos en explicarlas para no hacer interminable esta Kata):

~~~
.ssh/
extra/
machines/
.directory
*.iso
*.raw
*.qcow2
~~~

No cabe decir que este mismo contenido sería aconsejable que lo tuvieras también en el '.gitignore'.

Por tu cuenta podrías añadir unos test que comprobaran que están en los _ignore_ las carpetas/archivos indicados.


## Refactor: compartir las variables a través de archivos "yaml"

Este refactor nos evitará muchos dolores de cabeza. Con el uso de Ansible empezaremos a necesitar crear una gran cantidad de información que merece la pena tener visible toda junta ya que desde el primer test vamos a repetir información.

Tenemos el refactor del archivo "matrix_config.rb":

~~~
require 'yaml'

class Config
  CONFIGURATION = YAML.load(File.open('config/matrix_config.yml').read)

  def self.env
    data = {
      :garage => 'machines',
      :generic_name_of_a_docker_container => CONFIGURATION['generic_name_of_a_docker_container'],
      :matrix_user_one => CONFIGURATION['matrix_user_one'],
      :liveCD => CONFIGURATION['liveCD']
    }
    return data
  end
end
~~~

Y finalmente el archivo "config/matrix_config.yml":

~~~
---
generic_name_of_a_docker_container: 'matrix'
matrix_user_one: "Neo"
liveCD: "rancheros.iso"
~~~

Vuelve a lanzar los test: ¡Verde!

Con todo esto ya podemos pasar de nivel... vamos directamente a crear nuestras máquinas con Ansible.
