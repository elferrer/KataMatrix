# KataMatrix 1x16: Dieciseisavo nivel

En este nivel vamos a tener que configurar la infraestructura del clúster. Vamos a trabajar sobre el archivo 'spec/inside_a_vm/inside_a_vm.rb'.

Vamos a añadir a '/etc/hosts' las tres máquinas, para la kata nos va a servir que nuestro máster haga las veces de DNS, por lo que solo tendremos que dar de alta la IP en el archivo '/etc/hosts'. Crearemos primero el test:

~~~
context "the machine" do
  make = [Config.env[:nodeClusterOne], Config.env[:nodeClusterTwo], Config.env[:nodeClusterThree]]

  make.each do |creation|
    it "#{creation} is at '/etc/hosts'" do
      the_hosts = "/etc/hosts"
      remote_machine = creation

      etcHosts_content = File.read(the_hosts)

      expect(etcHosts_content).to include remote_machine
    end
  end
end
~~~

En rojo. Ahora debes añadir en el máster al archivo '/etc/hosts' las IP's:

~~~
ip.of.the.machine  neo-console-panel-one
ip.of.the.machine  neo-console-panel-two
ip.of.the.machine  neo-console-panel-three
~~~

Lanza los tests.


## Crear la llave dentro de neo-machine

Vamos a necesitar que la neo-machine controle por su cuenta su clúster, para ello deberá subir a cada uno de sus nodos la llave pública.

Primero los tests:

~~~
context "#{Config.env[:matrix_user_one]} has ssh keys in virtual machine " do
  key = SpecConfig.env[:masterCluster_key]
  home = ENV['HOME']
  safe_box_in_vm = "#{home}/#{SpecConfig.env[:safe_box]}"
  before(:all) do
    @softkey = "#{safe_box_in_vm}/#{key}"
  end

  make = ['', '.pub']
  make.each do |creation|
    it "has a #{creation} key" do
      initial_location = Dir.pwd
      Dir.chdir(safe_box_in_vm)

      content = Dir.glob("*")
      Dir.chdir(initial_location)

      expect(content).to include key+creation
    end
  end

  it "has a protected directory" do
    private_dir = File.new(safe_box_in_vm)

    private_properties = private_dir.stat
    dir_properties = "%o" % private_properties.mode

    expect(dir_properties).to eq SpecConfig.env[:permissions_dir_700]
  end

  it "has protected the ssh #{SpecConfig.env[:masterCluster_key]} private key" do
    private_key = File.new(@softkey)

    key_properties = private_key.stat
    file_properties = "%o" % key_properties.mode

    expect(file_properties).to eq SpecConfig.env[:permissions_file_600]
  end

  it "has protected the ssh #{SpecConfig.env[:masterCluster_key]}.pub public key" do
    private_key = File.new("#{@softkey}.pub")

    key_properties = private_key.stat
    file_properties = "%o" % key_properties.mode

    expect(file_properties).to eq SpecConfig.env[:permissions_file_644]
  end
end
~~~

Creamos la llave:

~~~
cd $HOME
chmod 700 .ssh
cd .ssh
ssh-keygen -b 2048 -t rsa -f neo_machine_key -q -N ""
chmod 600 neo_machine_key
chmod 644 neo_machine_key.pub
cd ..
~~~

Y lanzamos los tests.

Copiamos la llave a cada uno de los nodos del clúster... pero... primero los tests:

~~~
context "have a ssh port" do
  key = SpecConfig.env[:masterCluster_key]
  home = ENV['HOME']
  safe_box_in_vm = "#{home}/#{SpecConfig.env[:safe_box]}"
  before(:all) do
    @softkey = "#{safe_box_in_vm}/#{key}"
  end

  make = [Config.env[:nodeClusterOne], Config.env[:nodeClusterTwo], Config.env[:nodeClusterThree]]
  make.each do |remote_machine|
    it "open to #{remote_machine}" do
      remote_user = Config.env[:cluster_user]
      shell_sentence = "'echo 2>&1' && echo #{SpecConfig.env[:key_is_Ok]} || echo #{SpecConfig.env[:key_is_NOT_Ok]}"

      trying_open_door = launch_shellsentence_to_ssh_connection(@softkey,remote_user,remote_machine,shell_sentence)

      expect(trying_open_door).to include SpecConfig.env[:key_is_Ok]
    end
  end
end
~~~

y le mandamos nuestra llave:

~~~
ssh-copy-id -i ~/.ssh/neo_machine_key.pub rancher@neo-console-panel-one
~~~

Error!!! no podemos enviar la llave porque no tenemos acceso a las máquinas desde esta máquina.

Tenemos varias opciones:
1. Reactivar momentáneamente el acceso por usuario/contraseña.
2. Haber creado el disco duro sin haber cerrado el acceso por usuario/contraseña.
3. Traernos a esta máquina el par de llaves publica/privada que utilizamos en la fase anterior.

La más limpia es habilitar momentáneamente el acceso por usuario/contraseña en las tres máquinas para continuar con la tarea.

Añadimos el acceso por contraseña, en la tres VM:

~~~
#VM_paneles
sudo vi /etc/ssh/sshd_config
~~~

y cambiamos los valores siguientes para aceptar la entrada con usuario/contraseña por ssh:

~~~
PermitRootLogin yes
PasswordAuthentication yes
~~~

Realizamos un "restart" del servicio ssh en la tres máquinas VM.

~~~
#VM_paneles
sudo service ssh restart
~~~

Ahora podemos copiar la llave creada anteriormente. En una consola bash de la VM máster:

~~~
machine_list=( 'neo-console-panel-one' \
          'neo-console-panel-two' \
          'neo-console-panel-three' )
for machine in "${machine_list[@]}"
do
  ssh-copy-id -i ~/.ssh/neo_machine_key.pub rancher@$machine
done
~~~

Volvamos a quitar el acceso por contraseña, en las tres VM:

~~~
#VM_paneles
sudo vi /etc/ssh/sshd_config
~~~

y cambia los valores siguientes para denegar la entrada con contraseña por ssh:

~~~
PermitRootLogin no
PasswordAuthentication no
~~~

Realizamos un "restart" del servicio ssh en cada VM.

~~~
#VM_paneles
sudo service ssh restart
~~~

Te preguntarás porqué no hemos hecho un test. La razón es bien sencilla: ya estamos cubiertos. Si después de abrir el acceso por usuario/contraseña has lanzado los test desde tu máquina te habrán saltado en rojo los tests que lo comprueban. No es necesario duplicar unos tests que comprueban el mismo resultado.
