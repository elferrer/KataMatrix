# KataMatrix 1x15: Quinceavo nivel

Ya tenemos preparada la infraestructura del clúster. El siguiente paso es utilizar el nodo máster para crear el clúster. ¡Pero no sin mis tests!

Para poder continuar deberemos cerciorarnos que hemos copiado los tests a la VM... para ello nos aprovecharemos de los tests que tenemos creados.

Tenemos el tag "upload_test_to_machine" en los test que comprueban (y al mismo tiempo crean) los recursos/infraestructura necesarios para copiar toda la estructura de test dentro de una carpeta temporal en el nodo máster. Lancemos dichos tests:

~~~
rspec --tag upload_test_to_machine
~~~

Ahora conectemos a la máquina:

~~~
ssh -i .ssh/neo rancher@neo-machine
~~~

Situémonos dentro de la carpeta de tests y lancémoslos:

~~~
cd connectionToMatrixInGuest
rspec
~~~

¿Cuál ha sido el resultado?

Bueno... no está rojo... pero no ha lanzado toda la batería de tests. ¿Qué está ocurriendo?

Simplemente no estámos en la máquina real.

Así que lo primero que tenemos que hacer es preparar el entorno para que detecte que estamos dentro de una máquina virtual, lo cual es diferente a estar dentro de un entorno docker.

Vamos a desmontar/borrar la conexión que hemos preparado para subir los tests:

~~~
rspec --tag remove_tests_from_machine
~~~

Nota: el tag 'remove_tests_from_machine' borrará la carpeta temporal creada dentro de la VM, por lo que antes de lanzarlo debes asegurarte que no estás dentro de la carpeta temporal en la VM.

Para profundizar en cómo detectar diferentes tipos de virtualización puedes mirar este código: https://www.dmo.ca/blog/detecting-virtualization-on-linux/


## El test de la máquina virtual

Vamos a crear un nuevo archivo para los tests de la máquina virtual.

Pasito a pasito. Añadimos la llamada al test en el archivo 'matrix_spec.rb':

~~~
require 'inside_a_vm/inside_a_vm' if are_running_in_a_vm?
~~~

Lanzamos los tests (con el tag 'upload_test_to_machine').

Rojo. Atención al rojo. Añadimos la función 'are_running_in_a_vm?' a 'matrix.rb':

~~~
def are_running_in_a_vm?
  matrix? && system_user == 'rancher'
end
~~~

Ok, verde. Entramos en la VM y lanzamos los tests: rojo.

Desmontamos la conexión con el tag 'remove_tests_from_machine' (recuerda que no puedes estar situado dentro de la carpeta temporal de la VM).

Recuerda que el test en rojo nos avisaba que no teníamos el archivo que estábamos requiriendo, así pues: preparamos una carpeta llamada 'spec/inside_a_vm' y dentro creamos el archivo 'inside_a_vm.rb'.

Ya tenemos la estructura en verde. Ahora empecemos con el test, en el archivo 'spec/inside_a_vm/inside_a_vm.rb':

~~~
describe "Inside VM " do
  context "Matrix" do
    it "exist" do
      in_the_matrix = matrix?

      expect(in_the_matrix).to be true
    end
  end
end
~~~

Cada vez que añadas/cambies código deberás desmontar y volver a montar la conexión para probar el nuevo código en la VM. No lo repetiremos, damos por supuesto que ya eres consciente de ello.

Refactor de 'matrix.rb':

~~~
def matrix?
  return false if !is_running_docker? && !is_running_vm?
  true
end

def are_running_in_a_vm?
  matrix? && system_user == 'rancher'
end

def is_running_vm?
  load_kernel_option = `dmesg | grep -i virtual`
  load_kernel_option.include?('virtual NIC driver')
end
~~~

Este caso es un poco "verde"... hasta hace poco no era normal levantar una VM dockerizada... pero los tiempos van que vuelan y cada vez es más normal.

Ahora para hacer más cómoda la kata...

Como esto es una Kata, no hemos utilizado Rsync para sincronizar las carpetas, sino que hacemos una simple copia con Ruby. En un uso real utilizarías Rsync una vez hubieras realizado la conexión con 'sshfs' y configurado el '/etc/fstab'.

Nota: Puedes lanzar en la VM esta retafila para lanzar los test, te será más cómodo:

~~~
cd connectionToMatrixInGuest && rspec && cd
~~~

La razón por la que borramos la carpeta temporal cada vez es porque el directorio susodicho 'parece' que es el mismo pero su inode es diferente y nos dará un pete porque estamos en un lugar inaccesible. De esta forma nunca nos quedamos dentro del directorio... pero vamos, que si para lanzar los tests quieres ir entrando y saliendo del directorio es cosa tuya :)


## Ejercicio

Crea una conexión persistente (testeada).
