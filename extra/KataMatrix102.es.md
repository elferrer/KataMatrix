# KataMatrix 1x02: Segundo nivel

## Levantar docker y entrar en Matrix

¿Ya tienes un entorno docker creado?

¿No? Bueno, ¡pues vamos a ello!

Vamos a testear si tenemos las herramientas mínimas necesarias, sirva esto como ejemplo ya que lo correcto sería testear todas las dependencias. Si antes comprobamos a golpe de teclado si teníamos instaladas las herramientas necesarias, ahora como ya tenemos "Rspec" instalado vamos a testear la instalación de los programas que necesitamos.

Vamos a crear un test-looping con tirabuzón. Añadamos al archivo "spec/matrix_spec.rb" con el siguiente contenido:

~~~
describe "2. The docker world" do
  context "has installed some programs in a machine:" do
    installed_programs = ['docker', 'docker-compose']

    installed_programs.each do |program|
      it "the #{program} is installed" do

      found_program = system("which #{program} > /dev/null")

      expect(found_program).to be true
      end
    end
  end
end
~~~

Lanzamos los tests. ¿Qué tal? ¿En rojo?

Pues vamos a instalar docker. Lo primero, intentamos buscar información en internet e informarnos; lo instalamos y volvemos a lanzar los tests hasta que los tengamos en verde.

Pero como supongo que alguno de los presentes tendrá pereza voy a ir aligerando la carga de la Kata:

~~~
sudo apt-get -f install -y -q
sudo apt-get -q install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update -y -q
sudo apt-get install -y -q docker-ce
sudo groupadd docker
sudo usermod -aG docker $USER
sudo systemctl enable docker
curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` > docker-compose
sudo mv docker-compose /usr/local/bin/.
sudo chmod +x /usr/local/bin/docker-compose
~~~


## Testea que "Inside docker the Matrix exist"

Lo primero es preparar un test para comprobar que estamos dentro de un entorno docker, y claro, no tiene gracia si el test es diferente al que testea que estamos fuera de un entorno docker. Así que únicamente cambiaremos los títulos y haremos que la comprobación sea un "true". Añadamos al archivo "spec/matrix_spec.rb" el test:

~~~
describe "Inside docker" do
  context "the Matrix" do
    it "exist" do
      in_the_matrix = matrix?

      expect(in_the_matrix).to be true
    end
  end
end
~~~

Lanza los test. Está en rojo, bueno, a trabajar: crea un archivo "Dockerfile" para generar un docker. Pega el siguiente contenido:

~~~
FROM ruby:2.4.2

RUN apt-get update
RUN gem install bundler

ENV HOME=/home/matrix
RUN mkdir -p $HOME
ADD . $HOME
WORKDIR $HOME

RUN bundle install
~~~

¡Es sencillo de entender!

Una breve introducción a los comandos que hemos visto:

_FROM_: lee una imagen 'dockerizada' de https://hub.docker.com (no es habitual realizar un 'Docker From Scratch').

_RUN_: ejecuta un comando (en este caso de Bash).

_ENV_: crea una variable de entorno.

_ADD_: Añade/copia "." archivos desde el host (tu máquina) al "$HOME" en el contenedor. Existen diferencias entre utilizar 'COPY' y 'ADD' como sentencia, te conmino a que te documentes sobre estas diferencias.

_WORKDIR_: hmmm... ¿no crees que se referirá al 'WORKing DIRectory'?

Dicho archivo hace que docker descargue una imagen, la actualice, instale bundler, copie archivos al contenedor e instale dependencias.

Vamos a generar la imagen y lanzar los tests:

~~~
docker build -t matrix .
~~~

Ahora entremos en el contenedor:

~~~
docker run -it matrix /bin/bash
~~~

Lancemos Rspec.

¡Upsss!

¿Qué ha pasado? ¡Los tests que teníamos en verde ahora en rojo y el que teníamos en rojo está en verde! ¿Porqué?

Salgamos del entorno docker tecleando "exit".

Hemos preparado un "extra/CHEATSHEET.md" para tener a mano unas pocas anotaciones sobre algunos comandos docker.


## Varios entornos en los tests

En el primer test necesitamos que el archivo no existiera, pero, como ahora estamos lanzando el test dentro de un contenedor el archivo sí que existe.

Pero... ¿cómo creamos un test independiente del entorno?

Hay varias aproximaciones. Típicamente esto se hace separando cada entorno y teniendo en cada uno de ellos sus tests independientes... es una buena táctica pero nos deja sin _armas_ cuando empiece la guerra y no sepamos en qué mundo estamos...

Nosotros vamos a testear el entorno _antes_ de testear el entorno, de esta forma sabremos qué tests debemos lanzar.

En este momento tenemos los test de "outside_docker" en verde y el de "inside_docker" en verde... pero cada en su entorno. Para solucionarlo tendremos que refactorizar antes de continuar, porque, insisto: *solo un comando para todos los test: rspec*.

Vamos a crear los archivos 'inside_docker' y 'outside_matrix' con sus respectivos tests.

~~~
mkdir spec/outside_matrix spec/inside_docker -p
touch spec/outside_matrix/01_.rb
touch spec/outside_matrix/02_.rb
touch spec/inside_docker/inside_docker.rb
~~~

Abre el archivo "spec/matrix_spec.rb" y borra todos los test (pero guárdate una copia), reescribamos el nuevo código.

El nuevo código en "spec/matrix_spec.rb" es este:

~~~
require_relative '../matrix'

if !matrix?
  require_relative 'outside_matrix/01_'
  require_relative 'outside_matrix/02_'
end

require_relative 'inside_docker/inside_docker' if matrix?
~~~

el de 'spec/outside_matrix/01_.rb' es:

~~~
describe "1. The system is run at" do
  it "the real world" do
    in_the_matrix = matrix?

    expect(in_the_matrix).to be false
  end
end
~~~

el de 'spec/outside_matrix/02_.rb' es:

~~~
describe "2. The docker world" do
  context "has installed some programs in a machine:" do
    installed_programs = ['docker', 'docker-compose']

    installed_programs.each do |program|
      it "the #{program} is installed" do

      found_program = system("which #{program} > /dev/null")

      expect(found_program).to be true
      end
    end
  end
end
~~~

y el de 'spec/inside_docker/inside_docker_.rb' es:

~~~
describe "Inside docker" do
  context "the Matrix" do
    it "exist" do
      in_the_matrix = matrix?

      expect(in_the_matrix).to be true
    end
  end
end
~~~

Lanza los test en local (la máquina real):

~~~
rspec
~~~

¿Verde?

Ahora rehaz la imagen y entra en el contenedor:

~~~
docker build -t matrix .
docker run -it matrix /bin/bash
~~~

Lanza los test:

~~~
rspec
~~~

¡El test está verde!

Sal del entorno docker.

Fíjate que en este refactor no hemos reescrito ninguno de los tres test, ni siquiera hemos escrito nada nuevo de código... únicamente hemos implementado la capacidad de saber en qué entorno estamos para redigirnos hacia los tests de dicho entorno.


## Crear una nueva funcionalidad con docker-compose

Cuando empezamos a lanzar muchas imágenes docker vemos que es engorroso recordar todos los parámetros y al final terminamos haciéndonos ficheros que nos faciliten la vida. Para evitar esto existe docker-compose, el cual es un ejecutable que se encarga de convertir un archivo "yaml" en una línea que entienda docker.

Vamos a crear nuestro primer "docker-compose.yaml":

~~~
version: '3'
services:
  matrix:
    container_name: matrix
    build: .
    volumes:
     - .:/home/matrix
     - bundle:/usr/local/bundle
    command: bash -c "rspec"

volumes:
  bundle:
    driver: local
~~~

Podemos ver que creamos un servicio "matrix" con un contenedor "matrix", el cual lanzará un comando "bash".

También configuramos los volúmenes para poder compartir una carpeta entre el host y el guest.

Ya podemos lanzarlo:

~~~
docker-compose up --build
~~~

Puede que recibas un error de docker porque ya existe una imagen con el mismo nombre... documéntate, localiza la imagen y bórrala; después puedes volver a lanzar el docker-compose.

Los test se han lanzado automáticamente. ¡Y está en verde!


### Vamos a configurar Rspec para que nos muestre la información completa de los tests

Cuando lanzamos múltiples "spec" los títulos de los test desaparecen, para solucionarlo abrimos el archivo "spec_helper", buscamos la línea "config.files_to_run.one?". Activémosla... venga trabaja un poco, documéntate y actívala... que no tenga que hacerlo yo todo :)
