#Docker 101

Below a list of some tools to use in the console.


## Some tools

Search an image in https://hub.docker.com:

~~~
docker search
~~~

Download an image from https://hub.docker.com to your system:

~~~
docker pull ruby:2.4.2
~~~

Create a new image from your system:

~~~
docker build -t new_image .
~~~

List images installed in your system:

~~~
docker images
~~~

List all containers generated in your system:

~~~
docker ps -a
~~~

Create a tag/link from an image:

~~~
docker tag ruby:2.4.2 new_name_of_image
~~~

Crete a tag/link from an identifier (where a234c is the image ID):

~~~
docker tag a234c new_name_of_image
~~~

Launch an image:

~~~
docker run name_of_image
~~~

Launch an image and enter in their machine with an interactive terminal:

~~~
docker run -i -t name_of_image /bin/bash
~~~

Launch an image and launch a command in the machine:

~~~
docker run name_of_image /bin/bash -c "rspec"
~~~

## Docker-compose

If you want to link various images you need to create a farm of dockers. You can use link functionality of a docker, or, you can use docker-compose system integration.

By using a 'docker-compose.yaml' you create a farm integration of diferent dockers.

If you want to run this, first build the docker-compose image:

~~~
docker-compose build
~~~

And after up the docker-compose:

~~~
docker-compose up
~~~

## Docker's runs

If you have a container stopped, and you want to re-run this without generating a new container, launch:

~~~
docker exec name_of_container
~~~

## Other docker considerations

Your dockers are a hungry, this meants they want more and more and more space on your disk...

Search how can you delete the images, containers, volumes and networks.

To prune docker environment (except volumes):

~~~
docker system prune -a
~~~

If not, your system will explode.

This is the simple explanation of docker.
