# KataMatrix 1x01: Primer nivel

## ¿Está Matrix en tu mundo?

Hemos preparado esta Kata para adentrarnos en la experimentación de tecnologías emergentes. Para seguir la Kata solo necesitas un par de cosas: tu imaginación y una pizca de internet. No va a ser necesario que pagues a ningún proveedor de servicios.


## Abre tu mente, empecemos la Kata

Lo primero que debemos hacer para seguir la Kata es tener un sistema "GNU/Linux" con "Ruby". ¿Por qué? Para ser libres. Nosotros estamos utilizando la distro "KDE Neon", se apoya en "Ubuntu" y que es derivada de "Debian".

Así que hagamos la primera prueba. Ábrete la consola y lanza lo siguiente:

~~~
ruby -v
~~~

Si te ha devuelto la versión de "Ruby" que tienes instalado, entonces, efectivamente, tienes instalado "Ruby".

En caso contrario tendrás que instalarlo... empecemos la Kata: ¡manos a la obra!


### Trabajo en grupo

En esta kata vamos a trabajar en grupo, para ello tendremos que prepararnos un repositorio, aprenderemos lo importante que es seguir un flujo estable.


#### Gitlab

Vamos a trabajar con "Gitlab" por que su código es libre y además nos da acceso gratuitamente a infinitos e ilimitados repositorios públicos o privados.

Lo primero que debemos hacer es cerciorarnos que tenemos instalado el cliente "git" en nuestra máquina y que lo tenemos debidamente configurado:

~~~
git --version
~~~

Y si no está instalado... pues ya sabes...

Después date de alta en "Gitlab" (o donde te apetezca).

Si ya estás preparado empezaremos por crearnos el típico "Readme":

~~~
mkdir katamatrix && cd katamatrix
echo "# KataMatrix is in your mind" >> README.md
~~~

Inicializamos un proyecto "git":

~~~
git init
~~~

Y realizamos nuestro primer "commit":

~~~
git add README.md
git commit -m "First commit"
~~~

Ahora vamos a conectar nuestro repo local con "Gitlab":

~~~
git remote add origin https://gitlab.com/<USER>/<NEW_REPO.git>
~~~

Habrás observado que debemos indicar el nombre de usuario donde pone "<USER>" y el nombre del repositorio que hayamos elegido donde pone "<NEW_REPO.git>".

Y ya podemos subirlo ("push"):

~~~
git push -u origin master
~~~


#### Bundler

Vamos a prepararnos el entorno para entrar en la KataMatrix. Comprobamos que tenemos instalado "Bundler":

~~~
bundle -v
~~~

... y si no lo tienes...

~~~
gem install bundle
~~~

y si recibes un error de que no puedes instalar la gema, entonces prueba como superusuario, en nuestro caso:

~~~
sudo gem install bundle
~~~


#### Gemfile

Ahora vamos a definir las dependencias en el archivo Gemfile:

~~~
touch Gemfile
~~~

y le añadimos:

~~~
source 'https://rubygems.org'

gem 'rspec', '~> 3.7.0'
~~~

Guardamos y lanzamos:

~~~
sudo bundle install
~~~

Dependiendo de tu distribución linux es posible que no necesites lanzar con derechos de superusuario (o no) el comando "bundle". Realmente depende de si cuando instalaste "Bundler" con la gema lo tuviste que hacer como superusuario o como usuario normal.


## Wow ¡ya has realizado tu primer test!

¿Ves como no es tan difícil utilizar buenas prácticas?

Primero hemos testeado si teníamos las herramientas necesarias, lo cual nos ha permitido conocer el entorno que tenemos, y a continuación hemos instalado aquello que necesitábamos... ¡hemos conseguido realizar satisfactoriamente la incursión en esta KataMatrix!


## Es el momento de trabajar en grupo

Al ser la KataMatrix un trabajo de creación de infraestructura es bastante complicado repartir las tareas en diferentes grupos para que cada grupo haga una tarea diferente al del resto porque muchas de ellas se basan en infraestructura anteriormente montada.

Por tanto enfocaremos la kata en que cada pareja haga una versión de este código, lo siguiente es poner en común el código y el conocimiento en común entre todo el equipo. Habitualmente esta acción se toma todos los días antes de empezar el día de trabajo (la típica "daily") sobre el código que cada grupo a trabajado sabiendo que es conocimiento que los demás no tienen, pero, como vamos a trabajar sobre lo mismo utilizaremos la "daily" para elegir la mejor versión del código generado (o la mezcla de algunos de ellos).

De esta acción puede que salgan "concerns" para las sesiones de revisión de código, dudas forma de trabajar o roces a causa de las decisiones tomadas (esto habrá que llevarlo a la "retrospectivas").

Todo esto se verá reflejado en los diferentes "commits" que habréis ido haciendo, y como punto final (al poner en común todo el código) haréis otro "commit" del trabajo puesto en común.

Además de aquí saldrán los contenidos de los que hablaréis en la revisión de código (procurad que sea alguien ajeno al equipo) y en las retrospectivas grupales (también mejor alguien ajeno al grupo).

Este será un trabajo constante que no se va a ver explicitado en esta Kata.

Esta Kata no solo es adecuada para tomar una perspectiva de la infraestructura de un sistema sino también de la coordinación y trabajo en equipo ya que obliga al consenso contínuo día a día.


## Ya estamos preparados: empecemos la Kata

### Preparar el entorno de test

Inicializamos el entorno de test con Rspec:

~~~
rspec --init
~~~

Esto creará los archivos ".rspec" y "spec/spec_helper.rb".

Ahora creamos el primer archivo donde iremos escribiendo los tests:

~~~
touch spec/matrix_spec.rb
~~~

Y también el archivo de nuestra aplicación que interactuará con Matrix:

~~~
touch matrix.rb
~~~


## Primer nivel

### Tu primer test. Comprueba si "nuestro sistema está en el mundo real"

Edita "spec/matrix_spec.rb" y escribe tu primer test:

~~~
require_relative '../matrix'

describe "1. The system is run at" do
  it "the real world" do
    in_the_matrix = matrix?

    expect(in_the_matrix).to be false
  end  
end
~~~

Todo test debe tener diferenciadas tres partes:

Apartado "Arrange": en este apartado preparamos todo lo necesario para poder realizar el test.

Apartado "Act": en este apartado realizaremos las acciones necesarias para conseguir nuestro objetivo.

Y, finalmente, el apartado "Assert": en este apartado comprobaremos que obtenemos el resultado apetecido.

Habrás visto que en este primer test que hemos escrito no tenemos "Arrange", esto es porque no hemos necesitado preparar nada... la vida no es perfecta... a veces ocurre.

Lanzamos los tests:

~~~
rspec
~~~

¡El test está incompleto! El test nos ha devuelto errores que no son del "Assert" ("NoMethodError").


### Es el momento de escribir código

Edita "matrix.rb" y añádele:

~~~
def matrix?
end
~~~

Vuelve a lanzar los test.

¡Ahora sí! El test está rojo, ha fallado. Como habrás visto no es tan fiero el lobo como lo pintan... solo asusta un poco si no lo conoces.

Vamos a completar el método:

~~~
def matrix?
  File.file?('/.dockerenv')
end
~~~

Esto es muy importante, debemos seguir siempre el flujo correcto:

a. Escribir el test y confirmar que está rojo. Así nos aseguramos que si se rompe la aplicación el test nos lo indicará.

b. Escribir el código para conseguir pasar el test en verde.

c. Y finalmente refactorizar el test (primero) manteniendo el test en verde y después refactorizar el código manteniendo el test en verde.
