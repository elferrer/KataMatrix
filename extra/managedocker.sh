#!/bin/bash
#Author: Raül Martínez i Peris, raul@cuchame.es, elferrer.art
VAR=$1
ESP=$2
echo '\nTesting permissions of docker ... \n'
docker ps | 2>&1
DOCKERPERMISSION=$?
if [ $DOCKERPERMISSION -eq 0 ];then
   USER=''
   echo '\nYou not need sudo to use docker'
else
   USER='sudo'
   echo '\nYour docker need sudo... I automatically added to script.'
fi

echo '\n'

case $VAR in
--help)
printf "\nCommands:\n"
printf "  purge (purge all images, containers, volumes and networks)\n"
printf "  list (list images, containers, volumes and networks)\n"
printf "  clear (delete all containers, volumes and networks)\n"
printf "  erase name_of_container (delete only matching containers)\n"
printf "  \n"
printf "Author: Raül Martínez i Peris\n\n"
;;


purge)
echo "\nYou must enter the sudo password to calculate content of /var/lib/docker..."
sudo du -sh /var/lib/docker > /tmp/previous.docker
echo "... sudo password entered.\n"
echo "Containers removed..." && $USER docker rm -f $($USER docker ps -a -q) 2>/dev/null
echo "Images removed..." && $USER docker rmi -f $($USER docker images -q) 2>/dev/null
echo "Volumes removed..." && $USER docker volume rm $($USER docker volume ls -qf dangling=true) 2>/dev/null
echo "Networks removed..." && $USER docker network prune -f 2>/dev/null
echo "\nThe previous size of /var/lib/docker is:"
cat /tmp/previous.docker
echo "\nand the new size is:"
sudo du -sh /var/lib/docker
;;


erase)
echo 'Searching containers'
$USER docker ps -a | grep $ESP
EXISTYOURCONTAINER=$?
if [ $EXISTYOURCONTAINER -eq 0 ];then
  $USER docker ps -a | grep $ESP | xargs $USER docker rm
else
  echo 'Not coincidences'
fi
;;


clear)
$USER docker ps -a -q
EXISTCONTAINERS=$?
if [ $EXISTCONTAINERS -eq 0 ];then
  echo 'There are not containers'
else
  $USER docker rm -f $($USER docker ps -a -q)
fi
$USER docker volume prune -f
$USER docker network prune -f
;;


list)
printf "\nDocker images:\n"
$USER docker images
printf "\nDocker containers:\n"
$USER docker ps -a
printf "\nDocker volumes:\n"
$USER docker volume ls
printf "\nDocker networks:\n"
$USER docker network ls
;;

*)
printf "To show commands, use --help\n"
;;
esac
