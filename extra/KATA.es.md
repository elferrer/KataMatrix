# KataMatrix

En KataMatrix vamos a ir descubriendo algunos de los caminos de virtualización existentes. Para ello utilizaremos las metáforas que nos brinda Matrix para relacionarnos con diferentes realidades o entornos.

Una parte importante es que nunca deberemos lanzar diferentes tests según los entornos en los que estemos, sino que debemos ser capaces de que los test nos den la información que necesitemos de cada entorno.

La importancia de la KataMatrix estriba en la capacidad de construcción de los entornos y su entendimiento a través del descubrimiento.

Pensemos en este paseo virtual, en el cual Neo se ha tomado la píldora y deberá ir descubriendo todo lo que le rodea para poder llegar a controlarlo...

Después de esto solo nos queda decir que únicamente hay un comando: "rspec".


## Primer nivel

_Archivo de apoyo: "extra/KataMatrix101.es.md"_


### Prueba 1: Comprueba si "nuestro sistema está en el mundo real"

El primer test consiste detectar cuando estamos en un entorno real y no virtualizado dentro de un docker. Simple ¿verdad?


## Segundo nivel

_Archivo de apoyo: "extra/KataMatrix102.es.md"_

A partir de ahora vienen algunos niveles de construcción. Ya no vamos simplemente a "suponer" que tienes instalado algún programa. Vamos a testear la construcción del entorno.

Esto obliga a que no vamos a tener código ejecutable ya que vamos a ir instalando y configurando las herramientas necesarias en el sistema hasta que estemos preparados para el despliegue final, pero eso sí: ni una sola ejecución sin antes realizar un test que lo requiera.


### Prueba 2: "The docker world has installed some programs in a machine"

Ahora vamos a testear que tenemos instalado docker y docker-compose... primero el test y después la instalación. Obviamente la instalación es a mano y en tu sistema, el test solo comprueba que has conseguido el objetivo de tener las herramientas instaladas...


### Prueba 3: "Inside docker the Matrix exist"

Vamos a comprobar que podemos levantar un docker y enviarle un argumento/variable que persista dentro del docker y que crea la cuenta de usuario "Neo" dentro del entorno dockerizado... pero...

... ¿creías que esta Kata iba a ser fácil?...

Tendrás que lanzar "Rspec" tanto desde dentro como desde fuera del entorno docker... ¡y consigue el verde en ambos entornos!


## Tercer nivel

_Archivo de apoyo: "extra/KataMatrix103.es.md"_


### Prueba 4:  "2. The docker world exist and connection is possible" desde fuera de matrix

En este test debemos comprobar que existe una configuración para levantar un docker. Aunque la premisa no es que funcione (solo que exista la posibilidad), es evidente que merece realizar el esfuerzo. Te preguntarás si este test no se cumplirá ya... averígualo... sigue los pasos correctos: RGR.


### Prueba 5: "2. The docker world exist and has /home/matrix as workdir" desde fuera de matrix

Ahora sí debemos tener la certeza de que vamos a levantar un docker. De hecho el test consistirá en comprobar que dentro del docker levantado existe "/home/matrix" como directorio de trabajo (sin entrar en el entorno docker).


### Prueba 6: "2. The docker world exist and is protected by .dockerignore"

Es el momento de tomarse en serio la seguridad de tu entorno... para ello debemos asegurarnos que los ficheros no deseados no entran dentro de docker. De momento, te encargo esta tarea facílisima, ni siquiera es necesario que añadas nada al _ignore_ del docker.


### Prueba 7: si dentro de Matrix "User Neo exist"

Veamos si eres capaz de levantar una imagen docker pasándole el usuario "Neo" como argumento... y evidentemente primero el test dentro del docker.


## Cuarto nivel

_Archivo de apoyo: "extra/KataMatrix104.es.md"_


### Prueba 8: en la CI de Gitlab que "el usuario Neo ha desaparecido"

- Comprobar que en el entorno de Gitlab-ci el contenido de la variable ("Neo") no existe o está vacía.


## Quinto nivel

_Archivo de apoyo: "extra/KataMatrix105.es.md"_


### Prueba 9: por defecto en la máquina real "el usuario no es 'Neo'"

Debemos comprobar que cuando lanzamos los test en nuestro entorno, fuera de cualquier Matrix, el usuario que está logueado no es 'Neo'.

Y tampoco vendría mal empezar a pensar en un refactor...


## Sexto nivel

_Archivo de apoyo: "extra/KataMatrix106.es.md"_


### Prueba 10: "ansible" y las librerías de virtualización que necesites.

Por supuesto, testea antes de instalar.


### Prueba 11: "tenemos descargado un constructor de máquinas"... lo que comúnmente llamamos una ISO.

Vamos a utilizar "RancherOS" como sistema LiveCD, antes de descargar la ISO debes prepararte el test.


### Prueba 12: la ISO es la correcta.

Haz un test comprobando que el checksum de tu ISO y la indicada por RancherOS coinciden.


### Prueba 13: Crear las llaves del "Creador de llaves"

Debemos crear un par de juegos de llaves público/privada para las conexiones "ssh", además deberemos testear que tienen los derechos de acceso adecuados para evitar que sean alteradas.

En cuanto termines este nivel te aconsejamos un refactor para poder leer desde Ruby las variables desde un archivo "yaml" para así minimizar los errores por repetir continuamente los mismos valores.


## Séptimo nivel

_Archivo de apoyo: "extra/KataMatrix107.es.md"_

[Merece la pena leer la introducción del archivo de apoyo.]


### Prueba 14: "tenemos un disco virtual .qcow2"

Este test tiene su miga. Puedes optar por dos caminos:

a. Crear directamente el disco virtual.

b. Crear la máquina virtual indicando el disco virtual que queremos crear.

La mejor opción es testear paso a paso, por lo que optaríamos por la opción "a". Sin embargo en la vida real no vas a perder tiempo en hacer lo mismo dos veces, por lo que dejaremos que se la creación de la máquina virtual la que cree el disco duro. Pero bajo ningún de los dos casos dejaremos de testear que el disco lo tenemos antes de testear que la máquina existe.


### Prueba 15: "la máquina virtual está creada"

Testea la creación de una máquina virtual para el "Creador de llaves".


### Prueba 16: "tenemos una copia del disco .qcow2 en formato .raw"

Realizamos una copia del disco .qcow2 en formato .raw.


## Octavo nivel

_Archivo de apoyo: "extra/KataMatrix108.es.md"_


### Prueba 17: "se puede crear una conexión ssh"

Ahora testea que puedes realizar una conexión por ssh con la llave del "creador de llaves".


### Prueba 18: "asegura que la única forma de conectar sea por ssh"

Deberás deshabilitar cualquier forma de poder entrar a la máquina excepto por ssh.


## Noveno nivel

_Archivo de apoyo: "extra/KataMatrix109.es.md"_

### Prueba 19: "la máquina puede estar apagada"

Deberás poder pasar test diferenciados tanto si la máquina está enchufada o en cualquier otra circunstancia que pueda ocurrir... como apunte extra estaría bien que aprendieras a revivirla.


## Décimo nivel

_Archivo de apoyo: "extra/KataMatrix110.es.md"_


### Prueba 20: "El nombre de la máquina virtual existe en el sistema anfitrión"

Deberás testear si el DNS es capaz de resolver la dirección de la nueva máquina.


### Prueba 21: "El nombre de la máquina virtual existe en la VM"

Deberás testear si el nombre de la VM está dentro del DNS de la misma máquina.


### Prueba 22: "La máquina virtual tiene configurado los nameservers"

Testear que la VM tiene configurado los nameservers.


### Prueba 23: "La máquina virtual tiene configurado un gateway válido"

Testear que la VM tiene un gateway correctamente configurado.


### Prueba 24: "La máquina virtual tiene una dirección IP válida"

Testear que la VM tiene correctamente configurada su IP.


### Prueba 25: "La máquina virtual tiene una versión de docker correcta"

Testear que la VM tiene configurado una versión de docker que sea adecuada para continuar trabajando con ella.


## Onceavo nivel

_Archivo de apoyo: "extra/KataMatrix111.es.md"_

### Pruebas 26 a 38 (replicar pruebas 13 a 25)

En estas pruebas tendrás que hacer lo mismo que has hecho para el "Creador de llaves", pero, será para crear una nueva máquina que será utilizada como máster del cluster perteneciente a "Neo".


## Doceavo nivel

_Archivo de apoyo: "extra/KataMatrix112.es.md"_

### Pruebas 39 a 52 (replicar pruebas 13 a 25)

En estas pruebas tendrás que hacer lo mismo que has hecho para el "Creador de llaves" y para "Neo", pero en cambio, será para crear tres nuevas máquinas que serán utilizadas como cluster de "Neo". Tienes punto extra si testeas las tres máquinas con el mismo test, sin triplicarlo.


## Treceavo nivel

_Archivo de apoyo: "extra/KataMatrix113.es.md"_

### Prueba 53: conecta el anfitrión con la máquina virtual de "Neo"

Prepara a través de sshfs la capacidad de compartir archivos a través de una carpeta temporal entre el anfitrion y la máquina virtual de "Neo".


## Catorceavo nivel

_Archivo de apoyo: "extra/KataMatrix114.es.md"_

### Prueba 54: Instala las herramientas necesarias para lanzar los tests en la máquina virtual

Deberás testear desde la máquina anfitrión si la VM de "Neo" tiene instalado Ruby, RSpec y Bundler. Además deberás instalarlas.


## Quinceavo nivel

_Archivo de apoyo: "extra/KataMatrix115.es.md"_

### Prueba 54: Levantar los tests en la VM

En esta prueba deberás conseguir lanzar los tests desde dentro de la VM. Sin excusas. Evidentemente los tests queremos que sean diferentes a los del entorno real, los dockerizados o los de gitlab.


## Dieciseisavo nivel

_Archivo de apoyo: "extra/KataMatrix116.es.md"_


### Prueba 55: Añadir el clúster al DNS de la VM máster

Deberás añadir las tres máquinas del clúster a la VM máster para que el DNS las encuentre.


### Prueba 56: Crear un nuevo par de llaves público/privada

Crear un nuevo par de llaves público/privada en la VM máster y añadirla a cada VM del clúster.


## Diecisieteavo nivel

_Archivo de apoyo: "extra/KataMatrix117.es.md"_


### Prueba 57: Instala nginx

Deberás instalar Nginx.


### Prueba 58: Configura nginx

Deberás configurar Nginx.


### Prueba 59: Instalar Kubectl

Instala Kubectl.


### Prueba 60: Instalar Rke

Instala Rke.


### Prueba 61: Comprobar la configuración de Rke.

Comprueba que la configuración de Rke está correcta.


### Prueba 62: Comprobar la configuración de Kubectl

Comprueba que la configuración de Kubectl está correcta.


### Prueba 63: Instala Helm

Comprueba la instalación de Helm.


## Dieciochoavo nivel

_Archivo de apoyo: "extra/KataMatrix118.es.md"_


### Prueba 64: Configurar Kubectl/Tiller

Debes configurar el servicio Tiller.


### Prueba 65: Crear los certificados

Debes configurar un servicio para responder a los certificados y crear también los certificados.


### Prueba 66: Instala Rancher

Simplemente instala Rancher y confirma que lo tienes accesible por Web.


### Premio: Instala Grafana

Como broche final, con Rancher en funcionamiento y tu flamante clúster HA... instálate un Grafana.
