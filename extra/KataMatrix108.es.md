# KataMatrix 1x08: Octavo nivel

## Preparar el llavero del "Creador de llaves"

Vamos a necesitar un llavero donde guardar las recién creadas llaves del "Creador de llaves" y para ello... bueno, dejémonos de chácharas: tenemos que tener una unidad de disco donde almacenar las claves ssh. Para ello deberemos crear la máquina virtual y reutilizar su disco duro. Manos a la obra.

Añade un test para comprobar que el disco se crea satisfactoriamente:

~~~
describe "7. The VM machine '#{Config.env[:machine_template]}'" do
  it "has a original hard disk ('#{Config.env[:template_QCowDisk]}')" do
    hardkey = Config.env[:template_QCowDisk]
    Dir.chdir(Config.env[:garage])

    content = Dir.glob(hardkey)
    Dir.chdir('..')

    expect(content[0]).to eq hardkey
  end
end
~~~

Ahora seguimos con el test de comprobar si existe la máquina virtual:

~~~
  it "is created" do
    the_machine = Config.env[:machine_template]

    expect(list_of_vm).to include the_machine
  end
~~~

Lanza los tests, comprueba si tienes que añadir alguna variable de entorno... y ahora a trabajar.

Lo primero será crear el archivo de configuración para Ansible.

Vamos a agilizar el copy/paste. En el repo tienes los archivos ya preparados, cópialos en su sitio y analízalos:

- config/matrix_config.yml
- spec/matrix_spec_config.rb
- spec/outside_matrix/outside_matrix_config.rb
- cook/keysring_of_keymaker.yml

Estos archivos ya tienen toda la configuración terminada hasta el final de la KataMatrix.

Ahora toca cocinar:

~~~
ansible-playbook cook/keysring-of-keymaker.yml
~~~

Antes de continuar deberías mirar las "peculiaridades" de la utilización de archivos "yaml"... así aprenderás a tomar las debidas precauciones. Léete el siguiente enlace: http://danieltao.com/safe_yaml/

Tenemos ahora la máquina levantada, en breves momentos tendremos la consola preparada para una primera instalación. Como es habitual en cualquier LiveCD, levantar una máquina con ISO no es persistente en el sistema y aunque es totalmente operativa debemos realizar una instalación a disco duro. Después de instalarla en el disco duro prepararemos una consola con la distribución de nuestro gusto (RancherOS se instala con una consola Busybox que viene como 'default' y solo tiene persistencia de /opt y /home, si queremos persistencia de todo el sistema de ficheros debemos instalar otra consola).

Como a partir de ahora vamos a ir cambiando de la consola de tu máquina a la consola de la máquina virtual indicaremos con "#VM" cuando tienes que introducir las órdenes en la máquina virtual. Evidentemente no es necesario que vayas tecleando el comentario a diestro y siniestro :)

Vayamos al primer paso, una vez arrancada el LiveCD por primera vez instalaremos la distro al disco duro:

~~~
#VM
sudo ros install -d /dev/sda
~~~

Más fácil imposible. Solo te hará dos preguntas; la primera, que confirmes la instalación (ya que formateará por completo el disco duro); la segunda, si queremos reiniciar la máquina, pero... presta atención: en el menú de arranque debemos elegir "Autologin on tty1 and ttyS0" para volver a tener acceso a la consola.

Si estás utilizando 'libvirt' en el reinicio se habrá desactivado la imagen ISO y estarás arrancando directamente desde el disco duro, pero, compruébalo ya que si arrancas desde la ISO otra vez todo el trabajo será en balde y tendrás que volver a empezar. Si levantas la máquina con HyperV el CDROM continúa enganchado. Con otros tipos de virtualización no lo he testeado.

Una vez volvemos a estar dentro de la VM instalamos la consola de Ubuntu:

~~~
#VM
sudo ros console enable ubuntu
~~~

Reiniciamos la máquina (puedes utilizar el típico "sudo shutdown -r now") y volvemos a entrar con el "Autologin on tty1 and ttyS0".

Antes de lanzar el test acuérdate de requerir el nuevo archivo de test.

Habrás comprobado que tienes un test en verde y otro test en rojo.

¿Entonces qué mierdas hemos hecho?

... tranqui... lee el mensaje de error.

Anda, copia al archivo "matrix.rb" el código que necesitas:

~~~
def list_of_vm
  `virsh list --all`
end
~~~

Ahora sí, los dos test pasarán inclusive con la máquina apagada. Apágala y compruébalo.

Lo siguiente es preparar la máquina para poder acceder a ella por ssh y finalmente crear una copia de seguridad.

Para comprobar que podemos entrar en la máquina con la llave debemos saber cuál es su IP, afortunadamente sí sabemos que estamos en la red 'default' del entorno virtual (la hemos introducido en la creación):

~~~
describe "8. The machine  '#{Config.env[:machine_template]}' " do
  virtual_network = Config.env[:name_virt_network]
  the_machine = Config.env[:machine_template]
  key = OutsideVirtualizationConfig.env[:machine_template_key]
  before(:all) do
    @softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
  end

  context "have a ssh port" do
    it "open to #{key}" do
      ip_address = get_ip_from(the_machine,virtual_network)
      remote_user = Config.env[:cluster_user]
      shell_sentence = "'echo 2>&1' && echo #{SpecConfig.env[:key_is_Ok]} || echo #{SpecConfig.env[:key_is_NOT_Ok]}"

      trying_open_door = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

      expect(trying_open_door).to include SpecConfig.env[:key_is_Ok]
    end

  end
end
~~~

Lanzamos los tests. Están jodidos.

Completa las funciones que necesitas en el archivo "matrix.rb":

~~~
def list_of_shutoff_vm
  `virsh list --state-shutoff`
end

def list_of_running_vm
  `virsh list --state-running`
end

def get_ip_from(the_machine,virtual_network)
  the_mac_address = get_mac_address(the_machine,virtual_network)
  return :failure_in_name_resolution if the_mac_address.nil?
  ip_from_mac(the_mac_address)
end

def get_mac_address(the_machine,virtual_network)
  ip_address_list = get_vm_ip_list(virtual_network)
  info_vm = get_mac_address_list the_machine
  mac_address = mac_from_a_vm ip_address_list,info_vm
  return mac_address
end

def get_vm_ip_list(virtual_network)
  `virsh net-dhcp-leases "#{Config.env[:name_virt_network]}"`
end

def get_mac_address_list(the_machine)
  `virsh domiflist --domain #{the_machine} | egrep -o '#{Config.env[:mac_expression]}' | tr -d '\n'`
end

def mac_from_a_vm(ip_address_list,mac_address)
  their_ip_and_mac = ip_address_list.match /#{mac_address}\s*ipv4\s*(#{Config.env[:ip_expression]})/
  return their_ip_and_mac
end

def ip_from_mac(ip_address_and_mac)
  ip_address_from_mac = ip_address_and_mac.to_s
  ip = ip_address_from_mac[/(.*) (#{Config.env[:ip_expression]})/,2]
  return ip
end

def launch_shellsentence_to_ssh_connection(key,remote_user,ip_address,shell_sentence)
  `ssh -i #{key} -q -o 'BatchMode=yes' #{remote_user}@#{ip_address} #{shell_sentence}`
end

def ask_permissions_of_ssh_connection(ip_address,remote_user)
  shell_sentence = "echo 2>&1"
  `ssh -o 'BatchMode=yes' -o 'ConnectTimeout=5' #{remote_user}@#{ip_address} #{shell_sentence}`
end
~~~

Bien, volvamos a la VM. Para poder trabajar a distancia con la VM vamos a necesitar una contraseña (después habilitaremos el acceso solo con llave):

~~~
#VM
sudo passwd rancher
~~~

Vamos a necesitar la IP de la máquina:

~~~
#VM
ifconfig
~~~

Volvemos a nuestra consola y le mandamos nuestra llave:

~~~
ssh-copy-id -i .ssh/softkey rancher@<la.ip.de.la.máquina>
~~~

Ahora Comprobamos que podemos acceder solo con la llave, definamos el test dentro del mismo describe:

~~~
it "secured, with only publickey (without root user login)" do
  ip_address = get_ip_from(the_machine,virtual_network)
  remote_user = Config.env[:cluster_user]
  permission_only_have_a_publickey = "Permission denied (publickey)"

  check_door = ask_permissions_of_ssh_connection(ip_address,remote_user)

  expect(check_door).to include permission_only_have_a_publickey
end
~~~

Quitamos el acceso por contraseña, en la VM:

~~~
#VM
sudo vi /etc/ssh/sshd_config
~~~

y cambia los valores siguientes para denegar la entrada con contraseña por ssh:

~~~
PermitRootLogin no
PasswordAuthentication no
ChallengeResponseAuthentication no
UsePAM no
~~~

Revísalo bien, puede que estén repetidas (vas a tener que pelearte con "vi"). Realizamos un "restart" del servicio ssh en la máquina VM.

~~~
#VM
sudo service ssh restart
~~~

Vuelve a lanzar los test.

Como punto final solo nos resta crear una copia de la "hardkey" en formato "raw". Preparemos el test:

~~~
  it "has a raw copy of '#{OutsideVirtualizationConfig.env[:template_RawDisk]}' " do
    hardkey = OutsideVirtualizationConfig.env[:template_RawDisk]
    Dir.chdir(Config.env[:garage])

    content = Dir.glob(hardkey)
    Dir.chdir('..')

    expect(content[0]).to eq hardkey
  end
~~~

En el test especificamos que queremos un disco raw, vamos a tener que realizar el copiado del disco qcow2 y convertirlo a dicho formato. ¿Sabes porqué preferimos un disco raw? Tiene sus ventajas... y sus desventajas. Depende de lo que vayas a necesitar: documéntate.

Antes de proceder a crear una copia del disco qcow2 deberemos asegurarnos que no está en uso, para ello apagaremos la máquina virtual y continuemos con la conversión del disco:

~~~
sudo qemu-img convert machines/hardkey.qcow2 machines/hardkey.raw
~~~

Vuelve a levantar la máquina y lanza los tests.
