# KataMatrix 1x04: Cuarto nivel

## Smith en la CI

Si estás utilizando un repositorio en Gitlab tienes a tu alcance la posibilidad de utilizar una CI. Vamos a implementar nuestra CI, para ello empezamos creando el archivo '.gitlab-ci.yml':

~~~
before_script:
  - apt-get update -qq && apt-get install -y -qq
  - ruby -v
  - which ruby
  - gem install rspec
  - gem install bundler
  - bundle install --jobs $(nproc)  "${FLAGS[@]}"

test:
  script:
    - bundle exec rspec
~~~

Guardamos y lo subimos al repo... busca el resultado del "pipeline" en tu repo de Gitlab.

¡El test está en rojo! ¿Porqué?


### Comprueba que "Neo no recuerda su nombre"

Como tenemos un nuevo mundo para investigar, vamos a necesitar una nave diferente con la que navegar. Vamos a tener que realizar otro refactorentremos de lleno en él, para ello actualizamos "spec/matrix_spec.rb" para ser capaces de detectar este nuevo mundo. Antes de poder lanzar los tests tenemos que preparar el entorno:

~~~
require_relative '../matrix'


if are_running_outside_the_matrix?
  require_relative 'outside_matrix/01_'
  require_relative 'outside_matrix/02_'
end

require_relative 'inside_docker/inside_docker' if are_running_in_a_docker?

require_relative 'smith_in_matrix/smith_in_matrix' if are_running_in_a_CI?
~~~

Ahora añadimos a "matrix.rb":

~~~
def are_running_outside_the_matrix?
end

def are_running_in_a_docker?
end

def are_running_in_a_CI?
end
~~~

Y creamos un nuevo archivo 'spec/smith_in_matrix/smith_in_matrix.rb' con el contenido:

~~~
describe "In a Gitlab-CI environment" do
  context "the user" do
    it "Neo has dissapeared" do
      confused = nil
      remember_your_name = system_user

      expect(remember_your_name).to be confused
    end
  end
end
~~~

El test está en rojo. Escribe el código en "matrix.rb" para pasar el test:

~~~
def are_running_outside_the_matrix?
  !matrix?
end

def are_running_in_a_docker?
  is_running_docker? && system_user  && !system_logname
end

def are_running_in_a_CI?
  is_running_docker? && !system_user
end
~~~

Tres mundos: tres verdes.
