# KataMatrix 1x17: Diecisieteavo nivel

Ha llegado el momento de poner alas al clúster.


## Instalar programas necesarios en la VM máster.

Lo primero que necesitamos es Nginx. Añadimos el test a 'spec/inside_a_vm/inside_a_vm.rb' (dentro de su describe):

~~~
context "have installed" do
  it "Nginx" do
    shell_sentence = system("nginx -v 2> /dev/null")

    expect(shell_sentence).to be true
  end
end
~~~

Ahora toca instalarlo, primero deberás actualizar el sistema y posteriormente instalar nginx:

~~~
sudo apt update
sudo apt upgrade -y
sudo apt install nginx nginx-extras -y
~~~

En el siguiente test vamos a necesitar copiarnos unos ficheros de configuración para no eternizar la creación de los mismos en la kata. Por ello vamos a refactorizar el test "outside_matrix/19_.rb", vamos a la test "and copy tests to VM is possible" y le añadimos que copie el directorio "config_files"... supongo que ya sabes como lanzar los tests para que se recarguen, pero antes, revisa los archivos para que estén acordes a los nombres de las máquinas y las IP de tu sistema (adivino no soy).

Después del refactor lanza los test desde tu máquina para comprobar que se copia la carpeta.

Ahora tenemos que hacer el test que comprueba que la configuración nginx, añade al context anterior:

~~~
context "and Nginx service is configured" do
  the_nginx_conf = "/etc/nginx/nginx.conf"
  nginx_conf = File.read(the_nginx_conf)

  make = [Config.env[:nodeClusterOne], Config.env[:nodeClusterTwo], Config.env[:nodeClusterThree]]
  make.each do |remote_machine|
    it "to integrate #{remote_machine}" do
      the_machine = remote_machine

      ip_of_the_machine = get_the_ip_from_hosts(the_machine)

      expect(nginx_conf).to include(ip_of_the_machine)
    end
  end
end
~~~

Ahora copiemos la configuración y reiniciemos el servicio:

~~~
sudo mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.backup
sudo cp ~/connectionToMatrixInGuest/config_files/nginx.conf /etc/nginx/nginx.conf
sudo service nginx restart
~~~

Con esto ya tenemos configurado nginx, pero cuando se reinicie la VM quedará pendiente el inicio de nginx, por lo que tenemos que añadirlo a la configuración de RancherOS para que lo levante:

~~~
printf "runcmd:\n- sudo service nginx restart" | sudo tee -a /var/lib/rancher/conf/cloud-config.yml > /dev/null
~~~

Evidentemente no hemos testeado cada uno de los diferentes puntos sobre los que se apoya esta configuración. Dejamos a tu gusto testear si se ha incluido en el archivo "cloud-config.yml" la susodicha línea.

Si lanzas los test los verás en rojo. Necesitas una función de apoyo en el archivo 'matrix.rb':

~~~
def get_the_ip_from_hosts(the_machine)
  ip_and_host = `getent hosts #{the_machine}`
  ip = ip_and_host[/(#{Config.env[:ip_expression]}) (.*)/,1]
  return ip
end
~~~


## Instalar Kubectl, rke y helm

Empezamos con "Kubectl"; preparemos el test, añade el siguiente context:

~~~
context "have installed" do
  it "kubectl" do
    shell_sentence = system("kubectl version > /dev/null")

    expect(shell_sentence).to be true
  end
end
~~~

Ahora deberás instalar Kubectl:

~~~
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
~~~

Ahora comprueba a mano que kubectl está instalado:

~~~
kubectl version
~~~

Verás que tienes la instalación correcta pero que también recibes el error "The connection to the server localhost:8080 was refused - did you specify the right host or port?"

Mmmmhhh... mala suerte... tendremos que seguir otro método más 'arcano', reemplacemos el anterior test por el siguiente:

~~~
context "have installed" do
  it "kubectl" do
    shell_sentence = `kubectl version 2>/dev/null`

    expect(shell_sentence).to include ("Client Version")
  end
end
~~~

Hemos refactorizado el test 'al vuelo'...

Ponemos como ejemplo este test de cómo hay que ya que en el primer caso debido a no tener creado el archivo de configuración ('.kube/config') con todo configurado, entonces, kubectl se ve incapaz de resolver adecuadamente el puerto y otras características, por tanto, era imposible de conseguir un verde sin realizar otros tests... y eso no es bueno... ya lo sabes.

Ahora podemos continuar. Preparémonos para instalar "Rke", añade:

~~~
context "have installed" do
  it "rke" do
    shell_sentence = system("rke --version > /dev/null")

    expect(shell_sentence).to be true
  end
end
~~~

Vayamos a la instalación de "Rke":

~~~
curl -LO https://github.com/rancher/rke/releases/download/v0.1.17/rke_linux-amd64
chmod +x ./rke_linux-amd64
sudo mv ./rke_linux-amd64 /usr/local/bin/rke
~~~

Comprobamos la versión:

~~~
rke --version
~~~

y ahora añadimos este context al context anterior:

~~~
context "and rke service is configured" do
  the_cluster_yml = "#{ENV['HOME']}/cluster.yml"
  cluster_yml = File.read(the_cluster_yml)

  make = [Config.env[:nodeClusterOne], Config.env[:nodeClusterTwo], Config.env[:nodeClusterThree]]
  make.each do |remote_machine|
    it "to integrate #{remote_machine}" do
      the_machine = remote_machine

      ip_of_the_machine = get_the_ip_from_hosts(the_machine)

      expect(cluster_yml).to include(ip_of_the_machine)
    end
  end
end
~~~

El test ya nos advierte que no tenemos el archivo de configuración, deberemos crearlo. Por suerte ya lo hemos hecho:

~~~
sudo mv ~/connectionToMatrixInGuest/config_files/cluster.yml ~/cluster.yml
~~~

El siguiente test es el que comprueba que "Kubectl" tiene su archivo de configuración... lo cual también es necesario para poder pasar el test que hemos "acallado". Añade el siguiente context:

~~~
context "have configured" do
  it "kubectl" do
    kube_config = ENV['HOME'] + '/.kube/config'

    exist_kube_config = File.file?(kube_config)

    expect(exist_kube_config).to be true
  end
end
~~~

Lanza los tests. Está en rojo.

Y vamos a dejar que empiece la magia de Rke:

~~~
rke up
~~~

Esto tardara unos minutos...

A continuación crearemos el directorio donde "Kubectl" espera tener por defecto su configuración, y, copiaremos ahí la configuración que ha creado "Rke":

~~~
mkdir  ~/.kube
mv kube_config_cluster.yml ~/.kube/config
~~~

Lanza los test y comprueba que están en verde.

Ahora pasemos a instalar "Helm", primero el test:

~~~
context "have installed" do
  it "helm" do
    shell_sentence = system "bash -c  'helm version'"

    expect(shell_sentence).to be true
  end
end
~~~

Continuemos con la instalación de "Helm":

~~~
curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get > get_helm.sh
chmod 700 get_helm.sh
./get_helm.sh
rm get_helm.sh
helm init
helm repo update
~~~

Lanza los tests. Helm es un poco perezoso, puede que tengas que esperar un minutico para ver el test en verde.


## Ejercicio

Testea y crea un almacenamiento 'local storage' con kubernetes y que esté en HA.
