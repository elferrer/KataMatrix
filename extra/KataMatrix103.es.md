# KataMatrix 1x03: Tercer nivel

## Testea si existe la conexión

Neo está en el mundo real, sin embargo va a necesitar entrar en Matrix para buscar al "Oráculo".


### Testea la conexión

Primero debemos verificar la existencia de la conexión. Añade a "spec/outside_matrix/02_.rb":

~~~
context "exist and" do
  it "connection is possible" do
    connection = exist_docker_connection?

    expect(connection).to be true
  end
end
~~~

Lanza los tests para confirmar que están en rojo.


### Vamos a programar una funcionalidad

Necesitamos confirmar que existe el archivo 'Dockerfile', ya que éste es el hará posible la conexión con Matrix. Añade a "matrix.rb":

~~~
def exist_docker_connection?
  File.file?('./Dockerfile')
end
~~~

¿Verde?


## ¿Existe un hogar en Matrix?

Neo necesita saber donde está en Matrix, para ello testearemos el "workdir" del contenedor.


### Testea si la la conexión "workdir" es "/home/matrix"

Añade a "spec/outside_matrix/02_.rb":

~~~
  it 'has /home/matrix as workdir' , :docker => true do
    build_a_docker_image

    working_connection = load_docker_working_directory

    expect(working_connection).to eq "/home/matrix\n"
  end
~~~

En el test podemos ver claramente las tres "A's".

_Arrange_: Levanta la conexión.

_Act_: Lee el 'workdir' escrito en la configuración del contenedor docker.

_Assert_: Nos aseguramos (expect) de que obtenemos el resultado necesario.

Lanzamos los tests... y está en rojo.

Vamos a utilizar una funcionalidad de RSpec que nos facilitará la tarea de ir lanzando los test. Cuando tengamos una cantidad considerable querremos lanzar solo unos cuantos, para ello se pueden poner etiquetas en los tests. Habrás visto que hemos añadido "_, :docker => true_" en la línea del "it" antes del "do". con esto estamos indicándole a RSpec la etiqueta ("tag") de dicho test. Después nos será de gran ayuda.

Lanza los tests normalmente. Están en rojo.


### Inspeccionar la conexión

Necesitamos tener levantado el docker para poder testearlo.

Primero añade a "matrix.rb":

~~~
def build_a_docker_image
  system("docker build -t matrix . > /dev/null")
end
~~~

Es importante que sepamos la diferencia entre las diferentes formas que tiene Ruby de ejecutar comandos de shell, este resumen nos viene de perlas:

https://stackoverflow.com/questions/6338908/ruby-difference-between-exec-system-and-x-or-backticks

Ahora ya podemos preguntarle al contenedor por la información que necesitamos saber. Añade a "matrix.rb":

~~~
def load_docker_working_directory
  `docker inspect matrix | grep -m1 "WorkingDir" | cut -d '"' -f 4`
end
~~~

Lanza los tests.

Ha llegado el momento de tocar suelo, aunque ya hemos instalado y lanzado el docker vamos a comprobar la salud de éste. Es el momento de coger el control de la nave, Zion nos espera.

Lo primero que haremos es higienizar nuestras imágenes docker. Añadiremos un test que compruebe que solo se va generar las imágenes docker con el contenido necesario.

Al archivo "spec/outside_matrix/02_.rb" le añadimos:

~~~
it "is protected by .dockerignore" do
  is_protected = ".dockerignore"

  content = Dir.glob(is_protected)

  expect(content[0]).to eq is_protected
end
~~~

Lanzamos los tests y comprobamos que está en rojo.

Para pasar el test necesitamos crear un archivo ".dockerignore":

~~~
touch .dockerignore
~~~

De momento lo dejamos en blanco, en un siguiente paso retomaremos este archivo, aunque hacemos incapié en una característica de docker: docker recorre todo el árbol del directorio, por tanto, cualquier archivo o carpeta al que no tenga acceso de lectura va a ocasionar un error; dicho esto solo es cuestión de lógica que aunque no lo explicitemos sepas que los archivos a los que no tendrá acceso los añadas al dockerignore.


## Más actores en Matrix

De acuerdo... pero, ¿hay alguien más en Matrix?

Vamos a crear otros usuarios, bien a través del Dockerfile o pasándolos como argumentos.


### Testea si dentro de Matrix "User Neo exist"

Añade a "spec/inside_docker/inside_docker.rb":

~~~
context 'User' do
  it 'Neo exist' do
    user = system_user

    expect(user).to eq "Neo"
  end
end
~~~

Lanza "docker-compose up --build". El test está en rojo.

Bien, ya has visto como lanzar los test levantando a mano la imagen de docker, o bien, lanzando el docker-compose... a partir a ahora tú decides... el resultado será el mismo.


#### Ayuda en la configuración de docker

Si creas un usuario en el Dockerfile y generas la imagen con él, entonces, tu imagen tendrá un usuario 'non-root' (no confundas los usuarios de tu sistema con los usuarios dentro de docker). Pero, si creas el usuario interactivamente a través de los argumentos de docker, entonces, podrás utilizar 'root' en cualquier momento.


### Code, code, code...

Para añadir el usuario al entorno docker, añade a "matrix.rb":

~~~
def system_logname
  ENV['LOGNAME']
end

def system_user
  ENV['USER']
end
~~~

Si lanzas los test del docker verás que continuamos en rojo. Todavía no le hemos indicado a la imagen de docker el usuario que tendrá. Vamos a ello.

Añadimos al Dockerfile (al final del archivo):

~~~
ARG SOUL=Smith
ENV USER ${SOUL}

RUN useradd -ms /bin/bash $USER

USER $USER
~~~

Con esto preparamos el Dockerfile para crear un nuevo usuario mientras estamos en el proceso de 'build' de la imagen docker.

Vuelve a generar la imagen de matrix:

~~~
docker build --build-arg SOUL=Neo -t matrix .
~~~

¡Ya estás preparado para ser un alma libre!

~~~
docker run -it matrix /bin/bash
~~~

Ahora podemos ver en la línea de comandos que se ha identificado como "neo@[...]". La imagen se ha creado con "Neo".

Lanzamos los tests y confirmamos que están en verde.

Ahora vamos a juguetear un poco con docker...

Creamos diferentes imágenes. Para ello debemos hacer un "tag" (etiquetado) de la imagen de "neo" recién creada.

~~~
docker tag matrix neo
~~~

Ahora hagamos otra imagen, pero para "Trinity" y etiquetémosla:

~~~
docker build --build-arg SOUL=Trinity -t matrix .
docker tag matrix trinity
~~~

Comprueba:

~~~
docker images
~~~


## Necesitamos un refactor

En 'matrix.rb' hagamos los siguientes cambios:

~~~
def matrix?
  return false if !is_running_docker?
  true
end
~~~

Y añade al final del archivo dos nuevos métodos:

~~~
private

def is_running_docker?
  File.file?('/.dockerenv')
end

def exist_docker_connection?
  File.file?('./Dockerfile')
end
~~~


## Vamos a ver como subir nuestras imágenes docker a la nube

Para entenderlo mejor primero vamos a borrar todo lo que hemos estado haciendo y volveremos a crear las imágenes para así tener un vistazo global del trabajo hecho.

Vamos a BORRAR TODAS LAS IMÁGENES... ¿Has leído bien? Vamos a borrar todas las imágenes docker... si ya has trabajado anteriormente con docker y tienes alguna imagen no hagas este paso y borra únicamente las creadas anteriormente. No digas que no te he avisado.

~~~
docker rmi -f $(docker images -q)
docker rm -f $(docker ps -a -q)
docker volume rm $(docker volume ls -qf dangling=true)
docker network prune -f
~~~

El docker-compose fallará si lo lanzas ahora.

Antes de continuar ábrete una cuenta en https://hub.docker.com, es gratuita.

Vamos a recrear las imágenes:

~~~
docker build -t matrix .
docker build --build-arg SOUL=Neo -t matrix .
docker tag matrix <my_docker_account>/neo
docker build --build-arg SOUL=Trinity -t matrix .
docker tag matrix <my_docker_account>/trinity
~~~

Ahora las vamos a subir a la nube https://hub.docker.com:

~~~
docker login
docker push <my_docker_account>/neo
docker push <my_docker_account>/trinity
~~~

En el 'docker-compose.yaml', cambia "image: neo:latest" por "image: <my_docker_account>/neo", etc.

Después de los cambios tendrás algo parecido a lo siguiente:

~~~
version: '3'
services:
  matrix:
    container_name: matrix
    build: .
    volumes:
     - .:/home/matrix
     - bundle:/usr/local/bundle

  neo:
    container_name: neo
    image: <my_docker_account>/neo
    volumes:
     - .:/home/matrix
     - bundle:/usr/local/bundle
    command: bash -c "rspec"

  trinity:
    container_name: trinity
    image: <my_docker_account>/trinity
    volumes:
     - .:/home/matrix
     - bundle:/usr/local/bundle

volumes:
  bundle:
    driver: local
~~~
