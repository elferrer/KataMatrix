# KataMatrix 1x14: Catorceavo nivel

## Instalar herramientas a través de la conexión 'ssh'

En este nivel vamos a testear si la VM de "Neo" tiene instaladas las herramientas necesarias.

Primero vamos a ver si tenemos instalado Ruby:

~~~
describe "20. The virtual machine '#{Config.env[:masterCluster]}'"  do
  virtual_network = Config.env[:name_virt_network]
  the_machine = Config.env[:masterCluster]
  key = SpecConfig.env[:user_of_masterCluster_key]
  before(:all) do
    @softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
  end

  context "have installed" do
    it "Ruby" do
      ip_address = get_ip_from(the_machine,virtual_network)
      remote_user = Config.env[:cluster_user]

      shell_sentence = "ruby -v"
      ask_version = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

      expect(ask_version).to include "ruby"
    end
  end
end
~~~

Entramos en la máquina virtual de "Neo" y primero actualizamos los paquetes:

~~~
sudo apt update
sudo apt upgrade -y
~~~

Después ya podemos empezar con la instalación de Ruby:

~~~
sudo apt install ruby
~~~

Vemos si tenemos instalado "RSpec":

~~~
it "RSpec" do
  ip_address = get_ip_from(the_machine,virtual_network)
  remote_user = Config.env[:cluster_user]

  shell_sentence = "rspec -v"
  ask_version = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

  expect(ask_version).to include "RSpec"
end
~~~

Nos instalamos "Rspec":

~~~
sudo gem install rspec
~~~

Testeamos si tenemos instalado "Bundler":

~~~
it "Bundler" do
  ip_address = get_ip_from(the_machine,virtual_network)
  remote_user = Config.env[:cluster_user]

  shell_sentence = "bundle -v"
  ask_version = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

  expect(ask_version).to include "Bundler"
end
~~~

E instalamos "Bundler":

~~~
sudo gem install bundle
~~~
