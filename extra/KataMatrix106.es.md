# KataMatrix 1x06: Sexto nivel

## Comprueba el ecosistema que rodea matrix en el mundo real

En esta fase tenemos que preparar la máquina para tener en funcionamiento máquinas virtuales, para utilizaremos las librerías "libvirt" y también "ansible" para automatizar la creación de las máquinas virtuales.

Primero el test:

~~~
describe "4. I have installed some programs in a machine:" do
  installed_programs = ['ansible',
    'imvirt', 'libvirtd', 'qemu-x86_64', 'virt-manager', 'virsh', 'ssh']

  installed_programs.each do |program|
    it "the #{program} is installed" do

      found_program = system("which #{program} > /dev/null")

      expect(found_program).to be true
    end
  end
end
~~~

Recuerda requerir el archivo de test a "spec/matrix_spec.rb".

Después, tras comprobar aquellos programas que no tienes instalados, puedes instalarlos tan ricamente...

~~~
sudo apt-get install -y -q qemu-kvm bridge-utils virt-manager libvirt-bin libvirt-dev qemu-utils qemu libosinfo-bin ssh
sudo /etc/init.d/libvirt-bin restart
sudo adduser $USER libvirtd
sudo usermod -a -G libvirtd $USER
~~~

Si te apetece le vas añadiendo todas las dependencias al array. También puedes añadir otros test que comprueben que se ha añadido el usuario al grupo "libvirtd".


## Crear máquinas: descarga una ISO

Ya sabrás que antes de empezar a teclear la solución tienes que tener un test que verifique que vas por buen camino. ¿Qué tal si hacemos un test para comprobar que la descarga de la ISO ha sido satisfactoria?

~~~
describe "5. An ISO (machine constructor)" do
  it "is downloaded" do
    Dir.chdir(Config.env[:garage])

    content = Dir.glob(Config.env[:liveCD])
    Dir.chdir('..')

    expect(content[0]).to eq Config.env[:liveCD]
  end
end
~~~

Ya no voy a ir preparándotelo todo, así que incluye la llamada a los test, y, añade las nuevas variables en la clase que hemos creado. Esto no lo volveré a recordar... ya somos mayorcitos :=)

La ISO que vamos a utilizar la descargamos de: https://github.com/rancher/os/releases/

La descargamos a una carpeta y a continuar jugando:

~~~
mkdir machines
wget -O 'machines/rancheros.iso' -c 'https://github.com/rancher/os/releases/download/v1.4.3/rancheros.iso'
~~~

Lanza los tests y comprueba que todo va por buen camino.

Ahora necesitamos comprobar que tenemos la máquina en perfectas condiciones, sin vulnerabilidades. Para ello comprobaremos su integridad comprobando que el checksum es el correcto.

Añadimos al mismo 'describe' un nuevo test:

~~~
it "have got a valid checksum" do
  Dir.chdir(Config.env[:garage])

  checksum = `grep 'md5:' #{OutsideVirtualizationConfig.env[:liveCD_checksum]}`
  machine_check = "md5: " + `md5sum #{Config.env[:liveCD]}`
  Dir.chdir('..')

  expect(machine_check).to eq checksum
end
~~~

Como novedad en el test decirte que hemos añadido una nueva clase para las variables que solo deben estar en ese entorno de test. Para ello las crearás en el archivo "spec/outside_matrix/outside_matrix_config.rb" y lo requerirás dentro del "if are_running_outside_the_matrix?" del archivo "spec/matrix_spec.rb":

~~~
  require './spec/outside_matrix/outside_matrix_config'
~~~

Vemos el test en rojo.

ahora debemos bajarnos el archivo "iso-checksums.txt" de comprobación:

~~~
wget -O 'machines/iso-checksums.txt' -c 'https://github.com/rancher/os/releases/download/v1.4.3/iso-checksums.txt'
~~~

... y si no se ha actualizado eliminando la versión descargada todo irá sobre ruedas. Ante cualquier fallo del test deberás revisar la info de la web de RancherOS para tener la versión ISO correcta con el archivo checksum correcto. Si la forma de crear el archivo ha cambiado deberás refactorizar el test para que lea correctamente el checksum.
