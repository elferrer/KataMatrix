# KataMatrix 1x12: Doceavo nivel

El siguiente objetivo es crear el clúster HA, vamos a darle a la máquina de Neo tres flamantes paneles de mando.

Pero para no aburrirnos ya que será prácticamente igual que el nivel anterior, testearemos todo el cluster al unísono... sí las tres nuevas máquinas.

Vamos a preparar primero las tres unidades de disco:

~~~
describe "15. '#{Config.env[:matrix_user_one]}' has" do

  make = [Config.env[:nodeClusterOne_disk], Config.env[:nodeClusterTwo_disk], Config.env[:nodeClusterThree_disk]]
  make.each do |creation|
    it "created the '#{creation}' harddisk" do
      hardkey = "#{creation}"
      Dir.chdir(Config.env[:garage])

      content = Dir.glob(hardkey)
      Dir.chdir('..')

      expect(content[0]).to eq hardkey
    end
  end
end
~~~

Y creamos las copias de los discos:

~~~
hardkey_list=( 'neo_hardkey_panel_one' \
               'neo_hardkey_panel_two' \
               'neo_hardkey_panel_three' )
for hardkey in "${hardkey_list[@]}"
do
  cp machines/hardkey.raw machines/$hardkey.raw
done
~~~

Lanzamos los tests.

Es la hora de crear las tres VM, de una tajada. Primero el test (dentro del describe anterior):

~~~

  make = [Config.env[:nodeClusterOne], Config.env[:nodeClusterTwo], Config.env[:nodeClusterThree]]
  make.each do |creation|
    it "created the '#{creation}' machine" do
      neo_console = "#{creation}"

      expect(list_of_vm).to include neo_console
    end
  end
~~~

Crea un nuevo archivo llamado 'cook/neo_consoles.yml' con el siguiente contenido:

~~~
---
- name: Create a neo-consoles
  hosts: localhost
  vars_files:
    - ../config/matrix_config.yml

  tasks:

    - name: create neo-consoles
      command: >
              virt-install --connect qemu:///system
                --name {{ item.name }}
                --memory {{ neo_consoles.mem }}
                --vcpus {{ neo_consoles.cpus }}
                --hvm
                --network network={{ neo_consoles.virt_network }}
                --vnc
                --os-type {{ neo_consoles.os_type }}
                --disk size=10,path=../machines/{{ item.sda }}
                --cdrom ../machines/{{ liveCD }}
                --keymap es
                --noautoconsole
      with_items:
        - name: "{{ neo_console_one.name }}"
          sda: "{{ neo_console_one.sda }}"
        - name: "{{ neo_console_two.name }}"
          sda: "{{ neo_console_two.sda }}"
        - name: "{{ neo_console_three.name }}"
          sda: "{{ neo_console_three.sda }}"
~~~

ejecútalo:

~~~
ansible-playbook cook/neo-consoles.yml
~~~

Una vez estén las máquinas creadas, lanza los tests.

Vamos a testear si están apagadas/encendidas:

~~~
describe "16. The virtual machine"  do
  virtual_network = Config.env[:name_virt_network]

  make = [Config.env[:nodeClusterOne], Config.env[:nodeClusterTwo], Config.env[:nodeClusterThree]]
  make.each do |the_machine|
    case
    when list_of_shutoff_vm.include?(the_machine)
      it "·\033[33m(#{the_machine} is shut off)\033[0m" do

        expect(list_of_shutoff_vm).to include OutsideVirtualizationConfig.env[:VM_is_off]
      end

    when list_of_running_vm.include?(the_machine)
      key = SpecConfig.env[:user_of_masterCluster_key]
      key_maker = OutsideVirtualizationConfig.env[:machine_template_key]
      before(:all) do
        @softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
        @softkey_keymaker = "#{SpecConfig.env[:safe_box]}/#{key_maker}"
      end

      it "#{the_machine} is running" do
        ip_address = get_ip_from(the_machine,virtual_network)

        expect(list_of_running_vm).to include OutsideVirtualizationConfig.env[:VM_is_running]
      end
    end
  end
end
~~~

Apágalas, enchúfalas y ves comprobándolo lanzando los tests.

Continuemos. Los siguientes tests deberán actuar cuando las máquinas estén en funcionamiento.

Recuerda, ahora debemos quitar la llave del 'Creador de llaves' pero no sin antes tener la nuestra dentro de las tres máquinas, para ello primero testeamos si podemos abrir las máquinas con nuestra llave:

~~~
context "have a ssh port in the #{the_machine}" do
  it "and it's open to #{key}" do
    ip_address = get_ip_from(the_machine,virtual_network)
    remote_user = Config.env[:cluster_user]
    shell_sentence = "'echo 2>&1' && echo #{SpecConfig.env[:key_is_Ok]} || echo #{SpecConfig.env[:key_is_NOT_Ok]}"

    trying_open_door = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

    expect(trying_open_door).to include SpecConfig.env[:key_is_Ok]
  end
end
~~~

Ahora vamos a activar la consola de ubuntu y la contraseña (en el reinicio vuelve a entrar con Autologin):

~~~
#VM
sudo ros console enable ubuntu
sudo shutdown -r now
sudo passwd rancher
~~~

Y creamos el directorio ".ssh":

~~~
#VM
mkdir ~/.ssh
~~~

Ahora con 'ifconfig' averigua las IP que tienen adjudicada cada una de ellas y toma nota.

Volvemos a nuestra consola y les mandamos la llave:

~~~
ip_list=( 'ip.of.the.machine' \
          'ip.of.the.machine' \
          'ip.of.the.machine' )
for ip in "${ip_list[@]}"
do
  cat .ssh/neo.pub | ssh -i .ssh/softkey rancher@$ip "xargs -I '{}' echo '{}' >> ~/.ssh/authorized_keys"
done
~~~

Lanzamos los tests.

Ahora a quitar la llave del 'Creador de llaves', escribimos el test:

~~~
it "and it's closed to key_maker" do
  ip_address = get_ip_from(the_machine,virtual_network)
  remote_user = Config.env[:cluster_user]
  shell_sentence = "'echo 2>&1' && echo #{SpecConfig.env[:key_is_Ok]} || echo #{SpecConfig.env[:key_is_NOT_Ok]}"

  trying_open_door = launch_shellsentence_to_ssh_connection(@softkey_keymaker,remote_user,ip_address,shell_sentence)

  expect(trying_open_door).to include SpecConfig.env[:key_is_NOT_Ok]
end
~~~

Y ahora a borrar la llave.

En principio, y por seguridad, esto ya lo hicimos _antes_ de crear la copia raw de la imagen de disco, por lo que no debería ser necesario que lo hicieras, a no ser que el test te diga lo contrario:

~~~
ip_list=( 'ip.of.the.machine' \
          'ip.of.the.machine' \
          'ip.of.the.machine' )
for ip in "${ip_list[@]}"
do
  cat .ssh/softkey.pub | ssh -i .ssh/neo rancher@$ip "xargs -I '{}' sed -i 's#{}##g' ~/.ssh/authorized_keys"
done
~~~

Realizamos el test para comprobar que hemos quitado el acceso con usuario y contraseña:

~~~
it "secure, with only publickey (without root user login)" do
  ip_address = get_ip_from(the_machine,virtual_network)
  remote_user = Config.env[:cluster_user]
  permission_only_have_a_publickey = "Permission denied (publickey)"

  check_door = ask_permissions_of_ssh_connection(ip_address,remote_user)

  expect(check_door).to include permission_only_have_a_publickey
end
~~~

Recuerda lo que hicimos para quitar el acceso por contraseña, revisamos que en cada VM:

~~~
#VM
sudo vi /etc/ssh/sshd_config
~~~

los valores siguientes estén correctamente establecidos para denegar la entrada con usuario y contraseña por ssh:

~~~
ChallengeResponseAuthentication no
UsePAM no
PermitRootLogin no
PasswordAuthentication no
~~~

Realizamos un "restart" del servicio ssh en la máquina VM.

~~~
#VM
sudo service ssh restart
~~~

Vuelve a lanzar los test.


## Replicar la configuración de etc/hosts en la nueva máquina y VMs.

El próximo paso es añadir las IP al DNS.

Testeamos:

~~~
describe "17. The virtual machines"  do
  virtual_network = Config.env[:name_virt_network]
  make = [Config.env[:nodeClusterOne], Config.env[:nodeClusterTwo], Config.env[:nodeClusterThree]]
  key = SpecConfig.env[:user_of_masterCluster_key]
  before(:all) do
    @softkey = "#{SpecConfig.env[:safe_box]}/#{key}"
  end

  make.each do |the_machine|
    context "#{the_machine} exist" do
      it "and it's in the system" do
        the_hosts = "/etc/hosts"
        ip_address = get_ip_from(the_machine,virtual_network)

        to_search = `cat #{the_hosts} | grep '#{the_machine}'`

        expect(to_search).to include ip_address
        expect(to_search).to include the_machine
      end
    end
  end
end
~~~

Edita en tu máquina el archivo '/etc/hosts' y añade la siguiente línea:

~~~
ip.of.the.machine  neo-console-panel-one
ip.of.the.machine  neo-console-panel-two
ip.of.the.machine  neo-console-panel-three
~~~

Ahora debemos testear en cada una de la 'neo_console' la configuración de los DNS:

~~~
context "#{the_machine}" do
  it "it's on" do
    ip_address = get_ip_from(the_machine,virtual_network)
    the_hosts = "/etc/hosts"

    remote_user = Config.env[:cluster_user]
    shell_sentence = "'cat #{the_hosts}'"
    to_search = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

    expect(to_search).to include the_machine
  end
end
~~~

Editamos cada 'hosts' en cada VM y le configuramos a cada una su IP.

Lanzamos los tests.

Continuamos testeando los 'nameservers':

~~~
context "have a ssh port in the #{the_machine}" do
  it "and it's open to #{key}" do
    ip_address = get_ip_from(the_machine,virtual_network)
    expected_nameservers = "- 8.8.8.8\n- 8.8.4.4\n\n"

    remote_user = Config.env[:cluster_user]
    shell_sentence = "'sudo ros config get rancher.network.dns.nameservers'"
    ask_nameservers = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

    expect(ask_nameservers).to eq expected_nameservers
  end
end
~~~

Entramos a cada VM y le configuramos el nameserver:

~~~
#VM
sudo ros config set rancher.network.dns.nameservers [8.8.8.8,8.8.4.4]
~~~

Testeamos el gateway correcto (puedes consultarlo en la máquina virtual lanzando un 'route -n'):

~~~
it "and have a valid gateway" do
  ip_address = get_ip_from(the_machine,virtual_network)
  remote_user = Config.env[:cluster_user]

  shell_sentence = "\"/sbin/ip route | awk '/#{Config.env[:name_virt_network]}/ { print $3 }'\""
  ask_gateway = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)
  expected_gateway = ask_gateway.match(/#{Config.env[:ip_expression]}/).to_s
  expected_gateway = "Machine off" if expected_gateway == ""
  shell_sentence = "'sudo ros config get rancher.network.interfaces.eth0.gateway'"
  configured_gateway = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

  expect(configured_gateway).to include expected_gateway
end
~~~

y lo configuramos en cada VM:

~~~
#VM
sudo ros config set rancher.network.interfaces.eth0.gateway ip.of.the.gateway
~~~

Ahora vamos a testear la configuración de la IP:

~~~
it "and have a valid address ip" do
  ip_address = get_ip_from(the_machine,virtual_network)
  ip_range = "#{ip_address}/24\n"
  remote_user = Config.env[:cluster_user]

  shell_sentence = "'sudo ros config get rancher.network.dns.nameservers'"
  ask_nameservers = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)
  remote_user = Config.env[:cluster_user]
  shell_sentence = "'sudo ros config get rancher.network.interfaces.eth0.address'"
  configured_ip = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

  expect(configured_ip).to eq ip_range
end
~~~

Vamos yendo a cada VM y las configuramos:

~~~
#VM
sudo ros config set rancher.network.interfaces.eth0.dhcp false
sudo ros config set rancher.network.interfaces.eth0.address ip.of.the.machine/24
~~~

Aunque si hemos llegado a este punto es porque todo se ha resuelto satisfactoriamente, haremos una validación en la VM de que está correctamente configurado:

~~~
#VM
sudo ros config validate -i /var/lib/rancher/conf/cloud-config.yml
~~~

Finalmente comprobamos el motor docker:

~~~
it "and have a correct docker engine" do
  ip_address = get_ip_from(the_machine,virtual_network)
  docker_engine = "current  docker-18.06.3-ce\n"

  remote_user = Config.env[:cluster_user]
  shell_sentence = "\"sudo ros engine list | grep 'current'\""
  configured_engine = launch_shellsentence_to_ssh_connection(@softkey,remote_user,ip_address,shell_sentence)

  expect(configured_engine).to eq docker_engine
end
~~~

Y si es necesario fijamos el motor en la versión necesaria:

~~~
sudo ros engine switch docker-18.06.3-ce
sudo shutdown -r now
~~~

Volvemos a lanzar los tests.


## Ejercicio

Añade el test y haz los cambios necesarios para que cada una de las 'neo-consoles' tenga también la IP y el hostname del máster del clúster ('neo-machine').
