require './matrix_config'

def matrix?
  return false if !is_running_docker? && !is_running_vm?
  true
end

def exist_docker_connection?
  File.file?('./Dockerfile')
end

def are_running_outside_the_matrix?
  !matrix?
end

def are_running_in_a_docker?
  is_running_docker? && system_user && !system_logname
end

def are_running_in_a_CI?
  is_running_docker? && !system_user
end

def are_running_in_a_vm?
  is_running_vm?
end

def system_user
  ENV['USER']
end

private

def is_running_docker?
  File.file?('/.dockerenv')
end

def is_running_vm?
  load_kernel_option = %x[dmesg | grep -i virtual]
  load_kernel_option.include?('virtual NIC driver')
end

def the_matrix_user
  Config.env[:matrix_user_one]
end

def system_logname
  ENV['LOGNAME']
end
